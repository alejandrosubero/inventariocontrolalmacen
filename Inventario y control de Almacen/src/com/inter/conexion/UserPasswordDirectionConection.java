package com.inter.conexion;

public interface UserPasswordDirectionConection {


	default public String tablaName() {
		String tablaName = "databasetabla";
		return tablaName;
	}

	default public String databaseName(){
		String databaseName ="databasenames";
		return databaseName;
	}
	
	default public String password() {
		String password= "admin";
		return password;
	}
	
	default public String user() {
		String user= "root";
		return user;
	}
	
	default public String database() {
		String database= "almacen2";
		return database;
	}
	
	
	default public String urlInicial(){
		String url= "jdbc:mysql://localhost:3306?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		return url;
	}
	
	default public String urlAlmacen(){
		String url= "jdbc:mysql://localhost:3306/"+this.database()+"?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		return url;
	}
	

	default public String urlGenerico(String databasename){
		String url= "jdbc:mysql://localhost:3306/"+databasename+"?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		return url;
	}
	
	
	/*default public String password() {
		String password= "e49377d9";
		return password;
	}
	
	default public String user() {
		String user= "h6112587";
		return user;
	}
	
	default public String database() {
		String database= "catarindia123";
		return database;
	}
	
	
	default public String urlInicial(){
		String url= "jdbc:mysql://db4free.net:3306?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		return url;
	}
	
	default public String urlAlmacen(){
		String url= "jdbc:mysql://db4free.net:3306/catarindia123?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		return url;
	}*/
	

	
	/*default public String password() {
		String password= "iIjQku6gvs";
		return password;
	}
	
	default public String user() {
		String user= "9Ml0p69O2d";
		return user;
	}
	
	default public String database() {
		String database= "9Ml0p69O2d";
		return database;
	}
	
	
	default public String urlInicial(){
		String url= "jdbc:mysql://remotemysql.com:3306?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		return url;
	}
	
	default public String urlAlmacen(){
		String url= "jdbc:mysql://remotemysql.com:3306/9Ml0p69O2d?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		return url;
	}*/
	
	
	/*
	default public String password() {
		String password= "TE9dXJdy4gpxr0OWPbVo";
		return password;
	}
	
	default public String user() {
		String user= "urhssdaewzfygxut";
		return user;
	}
	
	default public String database() {
		String database= "bkgvhhtl83nekngvdmfq";
		return database;
	}
	
	
	default public String urlInicial(){
		String url= "jdbc:mysql://urhssdaewzfygxut:TE9dXJdy4gpxr0OWPbVo@bkgvhhtl83nekngvdmfq-mysql.services.clever-cloud.com:3306?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		return url;
	}
	
	default public String urlAlmacen(){
		String url= "jdbc:mysql://urhssdaewzfygxut:TE9dXJdy4gpxr0OWPbVo@bkgvhhtl83nekngvdmfq-mysql.services.clever-cloud.com:3306/bkgvhhtl83nekngvdmfq?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		return url;
	}*/
	
	
	
	
	
	
}
