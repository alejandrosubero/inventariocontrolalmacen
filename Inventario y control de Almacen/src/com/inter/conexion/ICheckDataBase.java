package com.inter.conexion;

public interface ICheckDataBase {

	public void checkDatabase(String nameDataBase);
	public void getDataBaseQuery(String query);
	//public void usarDataBase();
	public void usarDataBase(String databaseName);
	public void createbaseDate(String nameDataBase);	
	public void createbaseDatePrincipal(String nameDataBase);
	public void checkDatabaseName(String nameDataBase);
	
}
