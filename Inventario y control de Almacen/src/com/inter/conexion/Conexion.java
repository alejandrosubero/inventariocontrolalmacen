package com.inter.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public interface Conexion  extends UserPasswordDirectionConection {

	// metodo que devuelve una conecion a MySQL a la base de datos seleccion
	default public Connection startedConexion(Connection com, String url) {

		String user = this.user();
		String pass = this.password();
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			com = DriverManager.getConnection(url, user, pass);
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return com;
	}

	

	default public Statement createstament(Connection com) {

		Statement st = null;

		if (com != null) {
			try {
				st = com.createStatement();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return st;
	}

	
	default public void addDataBase(String query, Statement st) {

		if (st != null) {
			try {
				int count = st.executeUpdate(query);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	
	
	
	default public void closeConexion(Connection com, String url) {
		if (startedConexion(com, url) != null) {
			try {
				com.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

	}
	
	
	default public void closeStatement(Connection com, Statement st) {
		try {
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	default public void closeConexionPlus(Connection com, Statement st) {
		if (com != null) {
			try {
				com.close();
				closeStatementPlus(st);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

	}
	
	
	default public void closeStatementPlus(Statement st) {
		try {
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	default public Connection startedConexionPlus(Connection com, String url, Statement st) {
		
		closeConexionPlus(com,st);
		
		String user = this.user();
		
		String pass = this.password();
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			com = DriverManager.getConnection(url, user, pass);
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return com;
	}
	
	
	
	
	
	default public void accionConext(String urlDataBase, Connection com, Statement st) {
		com = this.startedConexion(com, urlDataBase);
		st = this.createstament(com);
	}
	
	
	default public void cerrarConext(String urlDataBase, Connection com, Statement st) {
		this.closeStatement(com, st);
		this.closeConexion(com, urlDataBase);
	}
	
	
	default public void resetConext(String nameDataBase, String newUrlDataBase,  String urlToclose,  Connection com, Statement st) { 
		this.cerrarConext(urlToclose, com, st);
		this.accionConext(newUrlDataBase, com, st);
	}
	
	
	
	
	
	
	
	
}
