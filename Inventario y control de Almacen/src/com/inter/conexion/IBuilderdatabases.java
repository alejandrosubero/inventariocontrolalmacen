package com.inter.conexion;

public interface IBuilderdatabases {
	
	public void usarDataBase();
	
	public void buildDataBase(String nameDataBase);
	
	public void builDataBaseAlmacen(String nameDataBase);
	
	public void builTableUser(String nameDataBase);
	
	public void builTableLicencia(String nameDataBase);
	
	public void builTableInventario(String nameDataBase);
	
	public void builTableInventarioSalida(String nameDataBase);
	
	public void builTableInventarioIngreso(String nameDataBase);
	
	public void builTableInventarioIngresoBorrado(String nameDataBase);
	
	public void builderDaseDatePrincipal(String nameDataBase);  
	public void builDataBasePrincipal(String nameDataBase);
	public void usarDataBasePrincipal(String nameBase);
	public void builPrincipalDatabaseTable(String nameDataBase);

}

