package com.inicio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.entidades.DataBaseName;
import com.entidades.User;
import com.inter.conexion.Conexion;
import com.inter.conexion.ICheckDataBase;
import com.inter.conexion.IUserConnection;
import com.inter.conexion.UserPasswordDirectionConection;

public class StarteLogin implements Conexion, ICheckDataBase,UserPasswordDirectionConection {

	private String databaseName;
	private String query = "";
	private String url = ""; //"jdbc:mysql://localhost:3306?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC"; // "jdbc:mysql://localhost:3306";

	private Statement statement;
	private Connection com;
	private BuilderDataBase builder;
	private DataBaseName dataBaseName = DataBaseName.getInstance();

	List<String> lista = new ArrayList<String>();
//	private LicenciaImple licencia = new LicenciaImple();

	public StarteLogin() {
		this.databaseName = this.database();
		this.url=this.urlInicial();
		this.com = this.startedConexion(com, url);
		this.statement = this.createstament(com);
	}

	public StarteLogin(String nameDataBases) {
		
		this.databaseName = nameDataBases;
		this.url= this.urlGenerico(nameDataBases);
		this.com = this.startedConexion(com, url);
		this.statement = this.createstament(com);
	}

	
	@Override
	public void checkDatabase(String nameDataBase) {
		if (databasePrincipalExist(nameDataBase)) {
//			accion("register",nameDataBase);
//			accion("licencia1",nameDataBase);
			accion("inventario",nameDataBase);
			accion("ingreso",nameDataBase);
			accion("editado",nameDataBase);
			accion("salida",nameDataBase);
		} else {
			createbaseDate(nameDataBase);
		}
	}


	@Override
	public void checkDatabaseName(String nameDataBase) { // ****
		if (databasePrincipalExist(nameDataBase)) {
			accion(this.tablaName(),nameDataBase);
			accion("register",nameDataBase);
			accion("licencia1",nameDataBase);

		} else {
			this.createbaseDatePrincipal(nameDataBase);
		}
	}
	
	
	public void verificaLicencia() {//no se a implementado a un a la fecha 27/10/2019.**************************
		usarDataBase(this.databaseName());
		LicenciaImple licencia = new LicenciaImple();
		licencia.checkLicencia();
		this.closeStatement(com, statement);
		this.closeConexion(com, url);
	}
	

	@Override
	public void getDataBaseQuery(String query) {
		try {
			ResultSet rt = statement.executeQuery(query);
			while (rt.next()) {
				lista.add(rt.getString(1));
			}
			rt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public void usarDataBase(String database) {
		if(database !=null) {
			query = "use "+database+" ;";
		}else {
			query = "use "+databaseName+" ;";
		}
		this.addDataBase(query, statement);
	}


	@Override
	public void createbaseDate(String nameDataBase) {
		builder = new BuilderDataBase();
		builder.buildDataBase(nameDataBase);
	}

	
	@Override
	public void createbaseDatePrincipal(String nameDataBase) {
		builder = new BuilderDataBase();
		builder.builderDaseDatePrincipal(nameDataBase);
	}
	
	
	
	public void updatelicencia(String Query) {	
		this.addDataBase(query, statement);
	}
	
	
	
	private boolean DatabaseExist() {
		boolean valor = false;
		lista =  new ArrayList<String>();
		query = "show databases;";
		getDataBaseQuery(query);
		for (String listado : lista) {
			if (listado.equals(databaseName)) {
				valor = true;
			}
		}
		return valor;
	}

	
	public boolean databasePrincipalExist(String databaseName) {
		lista =  new ArrayList<String>();
		boolean valor = false;
		query = "show databases;";
		getDataBaseQuery(query);
		for (String listado : lista) {
			if (listado.equals(databaseName)) {
				valor = true;
			}
		}
		return valor;
	}

	
	public boolean tablasPrincipalExist(String tabla, String databaseName) {
		
		boolean valor = false;
		usarDataBase(databaseName);
		query = "show tables;";
		getDataBaseQuery(query);
		for (String listado : lista) {
			if (listado.equals(tabla)) {
				return valor = true;
			}else {
				valor = false;
			}
		}
		return valor;
	}	
	
	
	
	public boolean tablasExist(String tabla, String databaseName) {
		boolean valor = false;
		usarDataBase(databaseName);
		url = this.urlAlmacen();  //"jdbc:mysql://localhost:3306/almacen?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		query = "show tables;";
		getDataBaseQuery(query);
		for (String listado : lista) {
			if (listado.equals(tabla)) {
				return valor = true;
			}else {
				valor = false;
			}
		}
		return valor;
	}

	
	
	/// funcionabilidad que pueda que se remueva dejandola solo a la base de datos
	public void accion(String tabla, String databaseName) {
		
		if (!tablasPrincipalExist(tabla, databaseName) ) {
			builder = new BuilderDataBase();
			builder.resetConexion(databaseName);
			switch (tabla) {
			case "register":
				builder.builTableUser(databaseName);
				break;
			case "licencia1":
				builder.builTableLicencia(databaseName);
				break;
			case "inventario":
				builder.builTableInventario(databaseName);
				break;
			case "ingreso":
				builder.builTableInventarioIngreso(databaseName);	
				break;
			case "editado":
				builder.builTableInventarioIngresoBorrado(databaseName);			
			     break;
			case "salida":
				builder.builTableInventarioSalida(databaseName);			
				break;
			case "databaseTabla":
				builder.builPrincipalDatabaseTable(databaseName);			
				break;
			default:
				break;
			}
			System.out.println("Se crea la tabla: " + tabla );// comentarios de pruebas 
			
		}else {
			System.out.println("La tabla "+ tabla +" Existe");// comentarios de pruebas 
		}
	}

}
