package com.inicio;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import javax.swing.JOptionPane;

import com.entidades.DataBaseName;
import com.entidades.Licencia;
import com.inter.conexion.Conexion;
import com.inter.conexion.UserPasswordDirectionConection;
import com.view.Login;
import com.view.licenciaView;

public class LicenciaImple implements Conexion, UserPasswordDirectionConection {

	private String duracion;
	private int duracion2;
	private String FirsDate;
	private LocalDate currentDate;
	private int mensajelicencia;
	private Licencia licencias;

	private Statement statement;
	private Connection com;
	private BuilderDataBase builder;

	private String databaseName;
	private String query = "";
	private String url = "";

	public LicenciaImple() {
		this.databaseName = this.databaseName();
		this.url = this.urlGenerico(databaseName);
		this.com = this.startedConexion(com, url);
		this.statement = this.createstament(com);
	}

//	public LicenciaImple() {
//		NumerosPatron patrones = new  NumerosPatron();
//	}
	
	public void checkLicencia() {
		if (!validarLicencia(getDataLicencia())) {
			licenciaView.startLicenciaView();
		} else {
			Login.StartVentana(databaseName);
		}
	}

	public void updateLicencia(String queryNew) {
		if (this.com != null) {	inicio(); }
			this.addDataBase(queryNew, statement);
			closeConexiones();
	}

	
	public Licencia getDataLicencia() {

		if (this.com != null) {	inicio(); }
			String FirsDate = "";
			try {
				ResultSet rt = statement.executeQuery("select * from licencia1;");
				while (rt.next()) {
					licencias = new Licencia(rt.getInt(1), rt.getString(2), rt.getLong(3), rt.getLong(4),rt.getString(5) );
				}
				rt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		
		closeConexiones();
		return licencias;
	}

	private void closeConexiones() {
		this.closeConexion(com, url);
		this.closeStatement(com, statement);
	}

	private void inicio() {
		this.databaseName = this.databaseName();
		this.url = this.urlGenerico(databaseName);
		this.com = this.startedConexion(com, url);
		this.statement = this.createstament(com);
	}

	public boolean validarLicencia(Licencia licencia) {
		boolean acceso = false;
		LocalDate date2 = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuu-MM-dd");
		LocalDate date1 = LocalDate.parse(licencia.getFechainicio(), formatter);
		long dayBetween = ChronoUnit.DAYS.between(date1, date2);
		// duracion=Long.toString(licencia.getDuracion());
		duracion2 = Integer.valueOf(Long.toString(licencia.getDuracion()));

		if (dayBetween < duracion2) {
			if ((duracion2 - dayBetween) < 11) {
				mensajelicencia = 0;
				acceso = true;
			} else {
				acceso = true;
				mensajelicencia = 1;
			}
		} else {
			mensajelicencia = 2;// debe de cargar la licencia y se abre la ventana de la licencia
		}
		return acceso;
	}

	// clave =14859522148595221485952214859522

	@SuppressWarnings("unlikely-arg-type")
	public String ingresaNewlicencia(String clave, Licencia licencia) {

		double n = 0.0;
		double patron1 = 0.0;
		String query = "";
		String legenda = "";

		if (clave.length() == 32) {
			if (licencia.getCodigo() == null || !clave.equals(licencia.getCodigo())) {
				for (int j = 0; j < clave.length(); j++) {
					if (j >= 16 && j < 25) {
						legenda += clave.charAt(j);
					}
				}

				for (int i = 0; i < legenda.length(); i++) {
					if (i >= 2 && i < 5) {
					//	n += Integer.valueOf(clave.charAt(i));
						n += Long.valueOf(clave.charAt(i));
					}
				}
				query = "UPDATE `licencia1` SET `duracion` = '" + n + "', `valor` = '" + patron1+ "', `codigo` = '" + clave + "' WHERE (`idlicencia1` = '1');";
				return query;
			}
		}
		return query;
	}

	public boolean Validausodeclave(String texto, int n) {
		double patron = ((n + (2 * n)) - n) * (5 * n);
		return false;
	}


	public String getFirsDate() {
		return FirsDate;
	}

	public String getDuracion() {
		return duracion;
	}

	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}

	public int getDuracion2() {
		return duracion2;
	}

	public void setDuracion2(int duracion2) {
		this.duracion2 = duracion2;
	}

	public void setFirsDate(String firsDate) {
		FirsDate = firsDate;
	}

	@Override
	public String toString() {
		return "Licencia [duracion=" + duracion + ", FirsDate=" + FirsDate + ", currentDate=" + currentDate + "]";
	}

}

//@SuppressWarnings("unlikely-arg-type")
//public String ingresaNewlicencia(String clave, Statement statement,Licencia licencia) {
//
//	int n=0;
//	double patron1=0;
//	String query="";
//	if (licencia.getCodigo() == null || !clave.equals(licencia.getCodigo())) {
//		
//		for (int j=0; j< clave.length(); j++) {
//
//			if (clave.charAt(j)==1 || clave.charAt(j)==2 || clave.charAt(j)==3) {
//				n = Integer.valueOf(clave.charAt(j));  
//				patron1 = ((n+(2*n))-n)*(5*n);
//				break;
//
//			}else if (clave.charAt(j)==4 || clave.charAt(j)==5 || clave.charAt(j)==6) {
//				n = Integer.valueOf(clave.charAt(j));  
//				patron1 = ((n+(2*n))-n)*(5*n);
//				break;
//			}else if (clave.charAt(j)==7 || clave.charAt(j)==8 || clave.charAt(j)==9) {
//				n = Integer.valueOf(clave.charAt(j));  
//				patron1 = ((n+(2*n))-n)*(5*n);
//				break;
//			}else if (clave.charAt(j)==0) {
//				n = Integer.valueOf(clave.charAt(j));  
//				patron1 = ((n+(2*n))-n)*(5*n);
//				break;
//			}
//		}

//		if (patron1 == 360.0 || patron1 == 490.0 || patron1 == 810) {
//			query= "UPDATE `licencia1` SET `duracion` = '180', `valor` = '"+patron1+"', `codigo` = '"+clave+"' WHERE (`idlicencia1` = '1');";
//			return query;	
//		}else if (patron1 == 1000.0) {
//			query= "UPDATE `licencia1` SET `duracion` = '360', `valor` = '"+patron1+"', `codigo` = '"+clave+"' WHERE (`idlicencia1` = '1');";
//		}
//	}
//			return query;		
//}
