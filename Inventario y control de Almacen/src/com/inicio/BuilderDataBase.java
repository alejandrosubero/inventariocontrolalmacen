package com.inicio;

import java.sql.Connection;
import java.sql.Statement;

import com.inter.conexion.Conexion;
import com.inter.conexion.IBuilderdatabases;
import com.inter.conexion.UserPasswordDirectionConection;

public class BuilderDataBase implements IBuilderdatabases, Conexion, UserPasswordDirectionConection{


	private Statement statement;
	private Connection com;
	private String query = "";
	private String url = this.urlInicial();//"jdbc:mysql://localhost:3306?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC"; // "jdbc:mysql://localhost:3306";
			
	
	public BuilderDataBase() {
		this.com = this.startedConexion(com, url);
		this.statement=this.createstament(com);
	}
	
	
	private void accion(String urlDataBase) {
		this.com = this.startedConexion(com, urlDataBase);
		this.statement = this.createstament(com);
	}
	
	
	private void cerrar() {
		this.closeStatement(com, statement);
		this.closeConexion(com, url);
	}
	
	
	public void resetConexion(String nameDataBase) {
		this.cerrar();
		url = this.urlGenerico(nameDataBase); 
		this.accion(url);
	}
	
	
	@Override
	public void buildDataBase(String nameDataBase) {
		builDataBaseAlmacen(nameDataBase);
		this.resetConexion(nameDataBase);
		this.usarDataBasePrincipal(nameDataBase);
		// builTableUser(nameDataBase);
		// builTableLicencia(nameDataBase);
		builTableInventario(nameDataBase);
		builTableInventarioSalida(nameDataBase);
		builTableInventarioIngreso(nameDataBase);
		builTableInventarioIngresoBorrado(nameDataBase);
		
		 this.cerrar();
	}

	
	@Override
	public void builDataBaseAlmacen(String nameDataBase) {
		query = "create database "+nameDataBase+";";
		this.addDataBase(query,statement);
	}

	@Override
	public void usarDataBase() {
		query = "use "+this.database()+";";
		this.addDataBase(query,statement);
	}
	
	
	@Override
	public void builderDaseDatePrincipal(String nameDataBase) {
		this.builDataBasePrincipal(nameDataBase);
		this.builPrincipalDatabaseTable(nameDataBase);
		this.builTableLicencia(nameDataBase);
		this.builTableUser(nameDataBase);
		this.cerrar();
	}
	
	@Override
	public void builDataBasePrincipal(String nameDataBase) {
		query = "CREATE DATABASE "+nameDataBase+";";
		this.addDataBase(query,statement);
	}
	
	@Override
	public void usarDataBasePrincipal(String nameBase) {
		query = "use "+nameBase+";";
		this.addDataBase(query,statement);
	}
	
	
	@Override
	public void builPrincipalDatabaseTable(String nameDataBase) {
		this.usarDataBasePrincipal(nameDataBase);
		//  url = this.urlAlmacen(); //"jdbc:mysql://localhost:3306/almacen?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		query = "CREATE TABLE databasetabla ( `id` INT NOT NULL AUTO_INCREMENT, `name` VARCHAR(45) NULL,  `clave` VARCHAR(200) NULL, `valor` VARCHAR(2000) NULL, fechacreada DATE NULL, PRIMARY KEY (`id`));";
		this.addDataBase(query,statement);
		
		query ="INSERT INTO databasetabla (name, clave, valor, fechacreada) values ('almacena','almacen','almacena', CURDATE());";
		this.addDataBase(query,statement);
	}
	
	
	
	@Override
	public void builTableUser(String nameDataBase) {
	//	url = this.urlAlmacen(); //"jdbc:mysql://localhost:3306/almacen?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		query = "CREATE TABLE register ( `id` INT NOT NULL AUTO_INCREMENT, `user` VARCHAR(45) NULL,  `password` VARCHAR(50) NULL, `rol` VARCHAR(45) NULL, PRIMARY KEY (`id`));";
		this.addDataBase(query,statement);
		
		query ="INSERT INTO register (user, password,rol) values ('admin','admin','admin');";
		this.addDataBase(query,statement);
		
		query ="INSERT INTO register (user, password,rol) values ('user','user','user');";
		this.addDataBase(query,statement);
		
		query ="INSERT INTO register (user, password,rol) values ('invitado','invitado','invaite');";
		this.addDataBase(query,statement);
	}

	

	@Override
	public void builTableLicencia(String nameDataBase) {
		// url =  this.urlAlmacen(); //"jdbc:mysql://localhost:3306/almacen?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		
	//	int idlicencia1, String fechainicio, long duracion, long valor, String codigo
		query = "CREATE TABLE licencia1 (`idlicencia1` INT NOT NULL AUTO_INCREMENT, `fechainicio` DATE NULL, `duracion` long NULL, `valor` long NULL, `codigo` VARCHAR(45) NULL, PRIMARY KEY (`idlicencia1`));";
		
		this.addDataBase(query,statement);
		query ="INSERT INTO licencia1 (fechainicio,duracion,valor,codigo) values (CURDATE(), 180, 180,'codigo');";
		// query ="INSERT INTO licencia1 (fechainicio,duracion) values (CURDATE(), 365);";
		this.addDataBase(query,statement);
	}

	@Override
	public void builTableInventario(String nameDataBase) {
	//	url =  this.urlAlmacen(); //"jdbc:mysql://localhost:3306/almacen?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		query = "CREATE TABLE inventario (`idinventario` INT NOT NULL AUTO_INCREMENT,`codigo` VARCHAR(45) NULL, `cantidad` INT NOT NULL," + 
				"  `almacenarea` VARCHAR(45) NULL, `almacenseccion` VARCHAR(45) NULL, `fechaultimoingreso` VARCHAR(45) NULL," + 
				"  `fechaultimasalida` VARCHAR(45) NULL, `nota` VARCHAR(100) NULL,  PRIMARY KEY (`idinventario`));";
		this.addDataBase(query,statement);
	}

	@Override
	public void builTableInventarioSalida(String nameDataBase) {
	//	url =  this.urlAlmacen(); //"jdbc:mysql://localhost:3306/almacen?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		query = "CREATE TABLE salida ( `idsalida` INT NOT NULL AUTO_INCREMENT, `codigo` VARCHAR(45) NULL, `cantidadsalida` INT NOT NULL," + 
				"  `almacenarea` VARCHAR(45) NULL, `almacenseccion` VARCHAR(45) NULL, `fechasalida` DATE NULL, `nota` VARCHAR(45) NULL, `cliente` VARCHAR(45) NULL," + 
				"  `usuario` VARCHAR(45) NULL, `rol` VARCHAR(45) NULL,  PRIMARY KEY (`idsalida`));";
		this.addDataBase(query,statement);
	}

	@Override
	public void builTableInventarioIngreso(String nameDataBase) {
	//	url =  this.urlAlmacen(); //"jdbc:mysql://localhost:3306/almacen?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		query = "CREATE TABLE ingreso (`idingreso` INT NOT NULL AUTO_INCREMENT,`codigo` VARCHAR(45) NULL,`cantidadrecibida` INT NOT NULL," + 
				"  `almacenarea` VARCHAR(45) NULL,  `almacenseccion` VARCHAR(45) NULL, `fechaingreso` DATE NULL, `nota` VARCHAR(100) NULL,`usuario` VARCHAR(45) NULL,`rol` VARCHAR(45) NULL," + 
				"  PRIMARY KEY (`idingreso`));";
		this.addDataBase(query,statement);
	}

	@Override
	public void builTableInventarioIngresoBorrado(String nameDataBase) {
	//	url = this.urlAlmacen(); // "jdbc:mysql://localhost:3306/almacen?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		query = "CREATE TABLE editado (`idingreso` INT NOT NULL AUTO_INCREMENT,`codigo` VARCHAR(45) NULL,`cantidadrecibida` INT NOT NULL," + 
				"  `almacenarea` VARCHAR(45) NULL,  `almacenseccion` VARCHAR(45) NULL, `fechaingreso` DATE NULL, `nota` VARCHAR(100) NULL,`usuario` VARCHAR(45) NULL,`rol` VARCHAR(45) NULL," + 
				"  PRIMARY KEY (`idingreso`));";
		this.addDataBase(query,statement);
	}

}





/*
 * �Est� a punto de DESTRUIR una base de datos completa! �Realmente desea ejecutar "DROP DATABASE `almacena`;
DROP DATABASE `databasenames`;
DROP DATABASE `nuebabase`;
DROP DATABASE `nuevabase`;
DROP DATABASE `vacamuerta`;
DROP DATABASE `valores`;"?

*/
