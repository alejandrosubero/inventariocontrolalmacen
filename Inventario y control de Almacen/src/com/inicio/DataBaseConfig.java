package com.inicio;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.entidades.DataBase;
import com.entidades.Inventario;
import com.entidades.Licencia;
import com.inter.conexion.Conexion;
import com.inter.conexion.UserPasswordDirectionConection;

public class DataBaseConfig implements Conexion, UserPasswordDirectionConection {

	
	private DataBase dataBase;
	private Statement statement;
	private Connection connection;
	List<String> lista = new ArrayList<String>();
	private String databaseName =this.databaseName();
	private String tablaName = tablaName();
	private String query = "";
	private String url = "jdbc:mysql://localhost:3306/" + databaseName
			+ "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	
	
	public DataBaseConfig() {
		this.connection = this.startedConexion(connection, url);
		this.statement = this.createstament(connection);
	}
	
		
	public void accion() {
		this.connection = this.startedConexion(connection, url);
		this.statement = this.createstament(connection);
	}
	
	
	public void cerrar() {
		this.closeStatement(connection, statement);
		this.closeConexion(connection, url);
	}

	
	
	public boolean tablasPrincipalExist(String tabla, String databaseName) {
		boolean valor = false;
		usarDataBase(databaseName);
		url = this.urlGenerico(databaseName);  
		query = "show tables;";
		getDataBaseQuery(query);
		for (String listado : lista) {
			if (listado.equals(tabla)) {
				return valor = true;
			}else {
				valor = false;
			}
		}
		return valor;
	}

	
	public void usarDataBase(String database) {
		if(database !=null) {
			query = "use "+database+" ;";
		}else {
			query = "use "+databaseName+" ;";
		}
		this.addDataBase(query, statement);
	}
	
	
	public void getDataBaseQuery(String query) {
		try {
			ResultSet rt = statement.executeQuery(query);
			while (rt.next()) {
				lista.add(rt.getString(1));
			}
			rt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public ArrayList<DataBase> workSpaceList() {	
		// accion();
		ArrayList<DataBase> bases = new ArrayList<>();
		if (connection != null) {
			try {
				String query = "SELECT * from "+tablaName+" ;";
				ResultSet rs = statement.executeQuery(query);
				while (rs.next()) {
					bases.add(new DataBase(rs.getInt("id"), rs.getString("name"), rs.getString("clave"), rs.getString("valor")));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		cerrar();
		return bases;
	}
	
	
	public void usarDataBasePrincipal(String nameBase) {
		query = "use "+nameBase+";";
		this.addDataBase(query,statement);
	}
	
	
	public void insertIntoDatabaseTabla(String newDataBase) {
		accion();
		this.usarDataBasePrincipal(databaseName);	
		query ="INSERT INTO databasetabla (name, clave, valor) values ('"+newDataBase+"','almacen','almacena');";
		this.addDataBase(query,statement);
		cerrar();
	}
	
	
	public String getTablaName() {
		return tablaName;
	}


	public void setTablaName(String tablaName) {
		this.tablaName = tablaName;
	}


	public DataBase getDataBase() {
		return dataBase;
	}


	public void setDataBase(DataBase dataBase) {
		this.dataBase = dataBase;
	}


	public Statement getStatement() {
		return statement;
	}


	public void setStatement(Statement statement) {
		this.statement = statement;
	}


	public Connection getConnection() {
		return connection;
	}


	public void setConnection(Connection connection) {
		this.connection = connection;
	}


	public String getDatabaseName() {
		return databaseName;
	}


	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}


	public String getQuery() {
		return query;
	}


	public void setQuery(String query) {
		this.query = query;
	}


	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}
	
	
	
}
