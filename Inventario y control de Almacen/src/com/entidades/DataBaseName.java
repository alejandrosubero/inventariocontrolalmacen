package com.entidades;

public class DataBaseName {

	private String databaseName;
	
	
	private static DataBaseName _instance = null;
	
	private DataBaseName() {}
	
	public static DataBaseName getInstance() {
	       if (_instance == null) {
	    	   _instance = new DataBaseName();
	       }
	       return _instance;
	   }
	
	
	@SuppressWarnings("static-access")
	public void resetInstancia() {
		this.databaseName = null;
		this._instance = null;
	}
	
	
	public String getDatabaseName() {
		return databaseName;
	}

	
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	
	@Override
	public String toString() {
		return "DataBaseName [databaseName=" + databaseName + "]";
	}
	
	
	
}


