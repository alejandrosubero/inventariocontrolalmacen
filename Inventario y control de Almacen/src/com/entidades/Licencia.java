package com.entidades;

public class Licencia {

	
	private int idlicencia1;
	private String fechainicio;
	private long duracion;
	private long valor;
	private String codigo;
	
	
	public Licencia(int idlicencia1, String fechainicio, long duracion) {
		super();
		this.idlicencia1 = idlicencia1;
		this.fechainicio = fechainicio;
		this.duracion = duracion;
	}
	
	
	public Licencia(int idlicencia1, String fechainicio, long duracion, long valor, String codigo) {
		super();
		this.idlicencia1 = idlicencia1;
		this.fechainicio = fechainicio;
		this.duracion = duracion;
		this.valor = valor;
		this.codigo = codigo;
	}


	public int getIdlicencia1() {
		return idlicencia1;
	}


	public void setIdlicencia1(int idlicencia1) {
		this.idlicencia1 = idlicencia1;
	}


	public String getFechainicio() {
		return fechainicio;
	}


	public void setFechainicio(String fechainicio) {
		this.fechainicio = fechainicio;
	}


	public long getDuracion() {
		return duracion;
	}


	public void setDuracion(long duracion) {
		this.duracion = duracion;
	}


	public long getValor() {
		return valor;
	}


	public void setValor(long valor) {
		this.valor = valor;
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + (int) (duracion ^ (duracion >>> 32));
		result = prime * result + ((fechainicio == null) ? 0 : fechainicio.hashCode());
		result = prime * result + idlicencia1;
		result = prime * result + (int) (valor ^ (valor >>> 32));
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Licencia other = (Licencia) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (duracion != other.duracion)
			return false;
		if (fechainicio == null) {
			if (other.fechainicio != null)
				return false;
		} else if (!fechainicio.equals(other.fechainicio))
			return false;
		if (idlicencia1 != other.idlicencia1)
			return false;
		if (valor != other.valor)
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "Licencia [idlicencia1=" + idlicencia1 + ", fechainicio=" + fechainicio + ", duracion=" + duracion
				+ ", valor=" + valor + ", codigo=" + codigo + "]";
	} 
	
	
	
	
	
}
