package com.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.entidades.DataBaseName;
import com.inicio.LicenciaImple;
import com.inter.conexion.Conexion;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.Connection;
import java.sql.Statement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class licenciaView extends JFrame  implements Conexion{

	private JPanel contentPane;
	private JTextField textField;
	private Statement statement;
	private Connection com;
	private String url;
	static licenciaView frame;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
				
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public static void startLicenciaView() {
		frame = new licenciaView();
		frame.setVisible(true);
	}
	/**
	 * Create the frame.
	 */
	public licenciaView() {
		inicio();
	}
	
	
	public void inicio () {
		setTitle("Licencia");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		setBounds(100, 100, 536, 288);
		
		JLabel lblLaLicenciaDel = new JLabel("La Licencia del programa esta vencida contacta proveedor");
		
		JButton CANCELAR = new JButton("Cancelar");
		CANCELAR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		
		JButton APLICAR = new JButton("Aplicar");
		APLICAR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				LicenciaImple lice = new LicenciaImple();
				String query = lice.ingresaNewlicencia(textField.getText(), lice.getDataLicencia());
				if (query.equals("")){
					lblLaLicenciaDel.setText("licencia no valida");
				}else {
					lice.updateLicencia(query);
					  lblLaLicenciaDel.setText("licencia aplicada");
					  frame.setVisible(false);
					  Login.StartVentana(DataBaseName.getInstance().getDatabaseName());
				}
			}
		});
		JLabel lblCodigoDeLicencia = new JLabel("Codigo de licencia");
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblIngreseCodigoDe = new JLabel("Ingrese codigo de licencia");
		lblIngreseCodigoDe.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblIngreseCodigoDe.setBounds(165, 25, 227, 33);
		contentPane.add(lblIngreseCodigoDe);
		
		textField = new JTextField();
		textField.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				
				APLICAR.setEnabled(true);	
				
			}
		});
		textField.setBounds(206, 77, 155, 22);
		contentPane.add(textField);
		textField.setColumns(10);
		
		
		lblCodigoDeLicencia.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblCodigoDeLicencia.setBounds(35, 78, 159, 19);
		contentPane.add(lblCodigoDeLicencia);
		
		
		APLICAR.setBounds(295, 162, 97, 25);
		APLICAR.setEnabled(false);
		contentPane.add(APLICAR);
		
		
		CANCELAR.setBounds(118, 162, 97, 25);
		contentPane.add(CANCELAR);
		
		
		lblLaLicenciaDel.setForeground(Color.RED);
		lblLaLicenciaDel.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblLaLicenciaDel.setBounds(25, 207, 493, 33);
		contentPane.add(lblLaLicenciaDel);
		
	}
}
