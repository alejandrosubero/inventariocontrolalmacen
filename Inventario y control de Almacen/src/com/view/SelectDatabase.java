package com.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.entidades.DataBase;
import com.entidades.DataBaseName;
import com.inicio.DataBaseConfig;
import com.inicio.StarteLogin;
import com.inter.conexion.Conexion;
import com.inter.conexion.ICheckDataBase;
import com.inter.conexion.UserPasswordDirectionConection;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Window;

import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;

public class SelectDatabase extends JFrame implements UserPasswordDirectionConection {

	static SelectDatabase frameSelectDatabase;
	private JPanel contentPane;
	private JTextField textField;
	private StarteLogin starteLogin = new StarteLogin();
	private DataBaseConfig databases;
	private JComboBox<String> comboBox = new JComboBox<String>();

	private boolean spaceWork;
	private String baseDedatos;
	private DataBaseName dataBaseName = DataBaseName.getInstance();
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				StartVentana();
			}
		});
	}

	
	public static void StartVentana() {
		try {
			frameSelectDatabase = new SelectDatabase();
			frameSelectDatabase.setVisible(true);
			frameSelectDatabase.setLocationRelativeTo(null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void checkDatabases() {
		
		if (starteLogin.databasePrincipalExist(this.databaseName())) {
			databases = new DataBaseConfig();
			if (databases.tablasPrincipalExist(this.tablaName(), this.databaseName())) {
				this.pullInComboBox();
			} else {
				starteLogin.accion(this.databaseName(), this.tablaName());
				this.pullInComboBox();
			}
		} else {
			starteLogin.checkDatabaseName(this.databaseName());
			databases = new DataBaseConfig();
			this.pullInComboBox();
		}
	}

	private void pullInComboBox() {
		List<DataBase> lista = new ArrayList<DataBase>();
		lista = databases.workSpaceList();
		
		for (DataBase base : lista) {
			comboBox.addItem(base.getName());
		}
		comboBox.setSelectedIndex(0);
		System.out.println("base de datos que voy a enviar " + lista.get(0).getName());
		baseDedatos = comboBox.getSelectedItem().toString();
		this.spaceWork = true;
	}

	/**
	 * Create the frame.
	 */
	public SelectDatabase() {
		
		this.comboBox.setVisible(false);
		 this.checkDatabases();

		setTitle("Selecci\u00F3n de la Base de datos");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 688, 428);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		comboBox.setVisible(true);
		textField = new JTextField();
		textField.setVisible(false);

		JButton openWorkSpace = new JButton("Abrir espacio de trabajo");
		openWorkSpace.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				boolean valor = !baseDedatos.equals("")?true:false;  
				
				if(valor) {
					dataBaseName.setDatabaseName(baseDedatos);
					BarraProgresoInicio1 barraProgreso = new BarraProgresoInicio1();
					barraProgreso.continueToLoguin(baseDedatos, valor);
					frameSelectDatabase.setVisible(false);
				}	
			}
		});

		JButton btnNewButton_1_1 = new JButton("Verificar si Existe");
		JButton buttoncancelarx = new JButton("X");
		JButton cancelar = new JButton("Cancelar");
		JLabel lblNewLabel = new JLabel("Selecione la una base de trabajo:");
		JButton btnNewButton = new JButton("Nueva Base de datos");

		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				baseDedatos = comboBox.getSelectedItem().toString();
			}
		});
		comboBox.setBounds(298, 129, 295, 21);
		contentPane.add(comboBox);
		buttoncancelarx.setVisible(false);

		btnNewButton_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("la base que quiere es: " + textField.getText().replaceAll(" ","") );
				boolean databaseExist = starteLogin.databasePrincipalExist(textField.getText().replaceAll(" ",""));
				if (databaseExist) {
					JOptionPane.showMessageDialog(null, "Existe la base de datos colocar otra ");
				} else {
					openWorkSpace.setText("crear y Abrir espacio de trabajo");
					int confirmado = JOptionPane.showConfirmDialog(	contentPane, "No existe la base de datos �Desea crearla?");
					if (JOptionPane.OK_OPTION == confirmado) {
						baseDedatos = textField.getText().replaceAll(" ","");
						databases.insertIntoDatabaseTabla(baseDedatos);
					     System.out.println("confirmado la base es " + baseDedatos );
					}else {
						openWorkSpace.setText("Abrir espacio de trabajo");
						textField.setText("");
					}	
				}		
			}
		});
		
		btnNewButton_1_1.setVisible(false);
		lblNewLabel.setBounds(63, 133, 225, 17);
		contentPane.add(lblNewLabel);

		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnNewButton.setVisible(false);
				textField.setVisible(true);
				btnNewButton_1_1.setVisible(true);
				buttoncancelarx.setVisible(true);
			}
		});
		btnNewButton.setBounds(252, 176, 166, 21);
		contentPane.add(btnNewButton);

		textField.setBounds(267, 219, 295, 19);
		contentPane.add(textField);
		textField.setColumns(10);

		openWorkSpace.setBounds(104, 300, 218, 21);
		contentPane.add(openWorkSpace);

		btnNewButton_1_1.setBounds(70, 218, 166, 21);
		contentPane.add(btnNewButton_1_1);

		buttoncancelarx.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openWorkSpace.setText("Abrir espacio de trabajo");
				textField.setVisible(false);
				btnNewButton_1_1.setVisible(false);
				buttoncancelarx.setVisible(false);
				btnNewButton.setVisible(true);
			}
		});

		buttoncancelarx.setBounds(572, 218, 55, 21);
		contentPane.add(buttoncancelarx);

		JLabel lblNewLabel_1 = new JLabel("Selecione el nombre del espacio de trabajo ");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNewLabel_1.setBounds(169, 35, 385, 29);
		contentPane.add(lblNewLabel_1);

		cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SelectDatabase ventana = new SelectDatabase();
				ventana.closePrograma(); 
			}
		});
		cancelar.setBounds(396, 300, 208, 21);
		contentPane.add(cancelar);
	}

	
	public void closePrograma() {
		System.exit(0);
	}
	

	public String getBaseDedatos() {
		return baseDedatos;
	}


	public void setBaseDedatos(String baseDedatos) {
		this.baseDedatos = baseDedatos;
	}
	
	
	
	
	
	
	
	
	
}
