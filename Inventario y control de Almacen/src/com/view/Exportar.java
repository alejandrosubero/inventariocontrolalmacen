package com.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import excel.LibrosExcel;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Exportar extends JFrame {

	private JPanel contentPane;
	private static String rol;
	private static String UserName;
	private String variable;
	
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Exportar frame = new Exportar();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	public Exportar() {
		inicio();	
	}

	
	
	public Exportar(String roll, String Name) {
		this.rol = roll;
		this.UserName = Name;
		inicio();
	}
	
	
	public void inicio() {
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 567, 329);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSelecioneLaTabla = new JLabel("Selecione la Tabla a imprimir");
		lblSelecioneLaTabla.setBounds(131, 16, 274, 21);
		lblSelecioneLaTabla.setFont(new Font("Tahoma", Font.BOLD, 17));
		contentPane.add(lblSelecioneLaTabla);
		
		
		String [] tablas = {"inventario", "salida", "editado","ingreso"};
		
		JComboBox comboBox = new JComboBox(tablas);
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				variable =comboBox.getSelectedItem().toString();
			}
		});
		comboBox.setBounds(131, 87, 274, 26);
		contentPane.add(comboBox);
		
		JButton imprimirButton = new JButton("Imprimir");
		imprimirButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				 try {
					 LibrosExcel inventariobook = new  LibrosExcel();
					 
					inventariobook.createBookInventario(variable,"" );
					
				} catch (Throwable e1) {
					e1.printStackTrace();
					System.out.println("ocurrio un error al crear el book");
				}
			}
		});
		

		imprimirButton.setBounds(290, 160, 115, 29);
		contentPane.add(imprimirButton);
		
		JButton ButtonSalir = new JButton("Salir");
		ButtonSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		ButtonSalir.setBounds(131, 160, 115, 29);
		contentPane.add(ButtonSalir);
		
	}
}
