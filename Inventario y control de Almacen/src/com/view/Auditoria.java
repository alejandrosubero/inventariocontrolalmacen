package com.view;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.view.panel.PanelAuditoriaComponentes;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

public class Auditoria extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4080498871356463715L;
	/**
	 * 
	 */

	private JTextField Ano;
	private JTextField mes;
	private JTextField dia;
	private JTextField ano2;
	private JTextField mes2;
	private JTextField dia2;
	private JTextField buscarpor1;
	private JTextField ano22;
	private JTextField mes22;
	private JTextField dia22;
	private JTextField ano23;
	private JTextField mes23;
	private JTextField dia23;
	private JTextField buscarpor2;
	private JTextField ano33;
	private JTextField mes33;
	private JTextField dia33;
	private JTextField ano32;
	private JTextField mes32;
	private JTextField dia32;
	private JTextField buscarpor3;
	private JTable table_Prueba;
	private JTextField textField_ano_prueba;
	private JTextField textField_mes_prueba;
	private JTextField textField_dia_prueba;
	private JTextField textField_ano2_prueba;
	private JTextField textField__mes2_prueba;
	private JTextField textField_dia2_prueba;
	private JTextField buscarpor_prueba;
	
	
	private JPanel contentPane;
	
	private DefaultTableModel model;
	private String rol, UserName;
	private JTable table;
	private JTable table_1;
	private JTable table_2;
	private JPanel panel_prueba1;
	private JPanel panel;
	private JPanel panel_1;
	private JScrollPane scroll;
	private JLabel lblNewLabel;
	private JPanel panel_2;
	private JPanel panel_3;

	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Auditoria frame = new Auditoria();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */

	public Auditoria() {
		setTitle("Auditar Inventario");
		getContentPane().setLayout(null);
		inicio();
	}

	public Auditoria(String rol, String UserName) {
		this.rol = rol;
		this.UserName = UserName;
		inicio();
	}

	private void closeSalir() {
		dispose();
	}

	private void inicio() {
		
		scroll =new JScrollPane();
		scroll.setBounds(MAXIMIZED_HORIZ, MAXIMIZED_VERT, MAXIMIZED_HORIZ, MAXIMIZED_VERT);
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1700, 729);
		setLocationRelativeTo(null);
		 
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setPreferredSize(new Dimension(1900, 900));
		
		scroll.setViewportView(contentPane);
		
		setContentPane(scroll);
		
	
		{
			lblNewLabel = new JLabel("  Auditar Inventario");
			lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 17));
		}
		
		JPanel panel_Base = new JPanel();
		
		PanelAuditoriaComponentes comp = new PanelAuditoriaComponentes(panel_Base);
		comp.panelDePrueba();
		
		panel_2 = new JPanel();
		PanelAuditoriaComponentes comp3 = new PanelAuditoriaComponentes(panel_2);
		comp3.panelDePrueba();
		
		panel_3 = new JPanel();
		PanelAuditoriaComponentes comp2 = new PanelAuditoriaComponentes(panel_3);
		comp2.panelDePrueba();
		
		
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(188)
					.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 199, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(10)
					.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 1655, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(10)
					.addComponent(panel_Base, GroupLayout.PREFERRED_SIZE, 1655, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(10)
					.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 1655, GroupLayout.PREFERRED_SIZE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
					.addGap(3)
					.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
					.addGap(2)
					.addComponent(panel_Base, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
					.addGap(3)
					.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE))
		);
		
		
		GroupLayout gl_panel_3 = new GroupLayout(panel_3);
		gl_panel_3.setHorizontalGroup(
			gl_panel_3.createParallelGroup(Alignment.LEADING)
				.addGap(0, 1655, Short.MAX_VALUE)
		);
		gl_panel_3.setVerticalGroup(
			gl_panel_3.createParallelGroup(Alignment.LEADING)
				.addGap(0, 210, Short.MAX_VALUE)
		);
		panel_3.setLayout(gl_panel_3);
		
		
		GroupLayout gl_panel_Base = new GroupLayout(panel_Base);
		gl_panel_Base.setHorizontalGroup(
			gl_panel_Base.createParallelGroup(Alignment.LEADING)
				.addGap(0, 1655, Short.MAX_VALUE)
		);
		gl_panel_Base.setVerticalGroup(
			gl_panel_Base.createParallelGroup(Alignment.LEADING)
				.addGap(0, 210, Short.MAX_VALUE)
		);
		panel_Base.setLayout(gl_panel_Base);
		
		
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGap(0, 1655, Short.MAX_VALUE)
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGap(0, 210, Short.MAX_VALUE)
		);
		panel_2.setLayout(gl_panel_2);
		contentPane.setLayout(gl_contentPane);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public void panelprueba() {
		panel_prueba1 = new JPanel();
		panel_prueba1.setBounds(15, 243, 1870, 210);
		panel_prueba1.setLayout(null);
		contentPane.add(panel_prueba1);
		PanelAuditoriaComponentes comp = new PanelAuditoriaComponentes(panel_prueba1);
		comp.panelDePrueba();
		//comp.componenteTabla();
		//comp.componentePanelControlPrueba();
	}
	
	
	public void panelprueba2() {
		panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(15, 243, 1870, 210);
		contentPane.add(panel);
		PanelAuditoriaComponentes comp3 = new PanelAuditoriaComponentes(panel);
		comp3.panelDePrueba();
		  // comp3.componenteTabla();
		   //comp3.componentePanelControlPrueba();
	}
	
	public void panelprueba3() {
		panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBounds(15, 243, 1870, 210);
		contentPane.add(panel_1);
		PanelAuditoriaComponentes comp2 = new PanelAuditoriaComponentes(panel_1);
		comp2.panelDePrueba();
		//comp2.componentePanelControlPrueba();
		//comp2.componenteTabla();
		//componentePanelControlPrueba(panel_1);
		//componenteTabla(panel_1);
	}
	

	
	public void componenteTabla(JPanel panel_prueba) {
		JScrollPane scrollPane_prueba = new JScrollPane();
		scrollPane_prueba.setBounds(477, 16, 574, 176);
		panel_prueba.add(scrollPane_prueba);
		table_Prueba = new JTable();
		scrollPane_prueba.setViewportView(table_Prueba);
	}
	
	public void componentePanelControlPrueba(JPanel panel_prueba) {
		JPanel panel = new JPanel();
		panel.setBounds(0, 16, 472, 183);
		panel_prueba.add(panel);
		panel.setLayout(null);
		JCheckBox checkBox_item_prueba = new JCheckBox("Buqueda por fecha");
		JCheckBox checkBoxfecha_prueba = new JCheckBox("Buqueda por item");
		checkBoxfecha_prueba.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {///////////////////////////////////////////////////77		
				if (checkBoxfecha_prueba.isSelected()) {
					checkBox_item_prueba.setSelected(false);
					buscarpor_prueba.setEnabled(false);
					textField_ano_prueba.setEnabled(true);
					textField_mes_prueba.setEnabled(true);
					textField_dia_prueba.setEnabled(true);
					textField_ano2_prueba.setEnabled(true);
					textField__mes2_prueba.setEnabled(true);
					textField_dia2_prueba.setEnabled(true);
					
				}
			}
		});
		checkBoxfecha_prueba.setBounds(73, 5, 157, 29);
		checkBoxfecha_prueba.setFont(new Font("Tahoma", Font.BOLD, 14));
		panel.add(checkBoxfecha_prueba);
		checkBox_item_prueba.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {////////////////////////////////////////////////////7777777
				
				if (checkBox_item_prueba.isSelected()) {
					checkBoxfecha_prueba.setSelected(false);
					buscarpor_prueba.setEnabled(true);
					textField_ano_prueba.setEnabled(false);
					textField_mes_prueba.setEnabled(false);
					textField_dia_prueba.setEnabled(false);
					textField_ano2_prueba.setEnabled(false);
					textField__mes2_prueba.setEnabled(false);
					textField_dia2_prueba.setEnabled(false);
				}
			}
		});
		checkBox_item_prueba.setBounds(235, 5, 163, 29);
		checkBox_item_prueba.setFont(new Font("Tahoma", Font.BOLD, 14));
		panel.add(checkBox_item_prueba);
		
		JLabel label = new JLabel("Fecha desde:");
		label.setFont(new Font("Tahoma", Font.BOLD, 14));
		label.setBounds(15, 49, 95, 20);
		panel.add(label);
		
		JLabel label_1 = new JLabel("A\u00F1o");
		label_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_1.setBounds(115, 49, 37, 20);
		panel.add(label_1);
		
		textField_ano_prueba = new JTextField();
		textField_ano_prueba.setEnabled(false);
		textField_ano_prueba.setColumns(10);
		textField_ano_prueba.setBounds(154, 46, 51, 26);
		panel.add(textField_ano_prueba);
		
		JLabel label_2 = new JLabel("Mes");
		label_2.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_2.setBounds(217, 46, 37, 20);
		panel.add(label_2);
		
		textField_mes_prueba = new JTextField();
		textField_mes_prueba.setEnabled(false);
		textField_mes_prueba.setColumns(10);
		textField_mes_prueba.setBounds(250, 43, 56, 26);
		panel.add(textField_mes_prueba);
		
		JLabel label_3 = new JLabel("Dia");
		label_3.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_3.setBounds(316, 49, 37, 20);
		panel.add(label_3);
		
		textField_dia_prueba = new JTextField();
		textField_dia_prueba.setEnabled(false);
		textField_dia_prueba.setColumns(10);
		textField_dia_prueba.setBounds(342, 43, 55, 26);
		panel.add(textField_dia_prueba);
		
		JLabel label_4 = new JLabel("Fecha hasta:");
		label_4.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_4.setBounds(15, 88, 95, 20);
		panel.add(label_4);
		
		JLabel label_5 = new JLabel("A\u00F1o");
		label_5.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_5.setBounds(115, 88, 37, 20);
		panel.add(label_5);
		
		textField_ano2_prueba = new JTextField();
		textField_ano2_prueba.setEnabled(false);
		textField_ano2_prueba.setColumns(10);
		textField_ano2_prueba.setBounds(154, 85, 51, 26);
		panel.add(textField_ano2_prueba);
		
		JLabel label_6 = new JLabel("Mes");
		label_6.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_6.setBounds(217, 85, 37, 20);
		panel.add(label_6);
		
		textField__mes2_prueba = new JTextField();
		textField__mes2_prueba.setEnabled(false);
		textField__mes2_prueba.setColumns(10);
		textField__mes2_prueba.setBounds(250, 82, 56, 26);
		panel.add(textField__mes2_prueba);
		
		JLabel label_7 = new JLabel("Dia");
		label_7.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_7.setBounds(316, 88, 37, 20);
		panel.add(label_7);
		
		textField_dia2_prueba = new JTextField();
		textField_dia2_prueba.setEnabled(false);
		textField_dia2_prueba.setColumns(10);
		textField_dia2_prueba.setBounds(342, 82, 55, 26);
		panel.add(textField_dia2_prueba);
		
		JLabel label_8 = new JLabel("Buscar por Item:");
		label_8.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_8.setBounds(15, 124, 129, 20);
		panel.add(label_8);
		
		buscarpor_prueba = new JTextField();
		buscarpor_prueba.setEnabled(false);
		buscarpor_prueba.setColumns(10);
		buscarpor_prueba.setBounds(137, 121, 146, 26);
		panel.add(buscarpor_prueba);
		
		JComboBox comboBox_prueba = new JComboBox();
		comboBox_prueba.setBounds(66, 157, 115, 26);
		panel.add(comboBox_prueba);
		
		JLabel label_9 = new JLabel("Tabla:");
		label_9.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_9.setBounds(15, 160, 69, 20);
		panel.add(label_9);
		
		JButton button_buscar_prueba = new JButton("Buscar");
		button_buscar_prueba.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		button_buscar_prueba.setBounds(353, 151, 104, 29);
		panel.add(button_buscar_prueba);
				
	}
	
	
	
	
	
	
	
	public void control1() {
		
		
		
		JLabel lblControlDeInventario = new JLabel("Control de Inventario");
		lblControlDeInventario.setFont(new Font("Tahoma", Font.BOLD, 16));
		JCheckBox chckbxBuquedaPorFecha = new JCheckBox("Buqueda por fecha");
		JCheckBox chckbxBuquedaPorItem = new JCheckBox("Buqueda por item");
		lblControlDeInventario.setBounds(159, 16, 187, 20);
		contentPane.add(lblControlDeInventario);

		
		JPanel panel = new JPanel();
		panel.setBounds(15, 36, 462, 189);
		contentPane.add(panel);
		panel.setLayout(null);

		
		JLabel lblTablas = new JLabel("Tabla:");
		lblTablas.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblTablas.setBounds(15, 167, 69, 20);
		panel.add(lblTablas);
		
		
		chckbxBuquedaPorFecha.addActionListener(new ActionListener() {///////////////////////////////////////////////////
			public void actionPerformed(ActionEvent arg0) {
				if (chckbxBuquedaPorFecha.isSelected()) {
					chckbxBuquedaPorItem.setSelected(false);
					buscarpor1.setEnabled(false);
					Ano.setEnabled(true);
					mes.setEnabled(true);
					dia.setEnabled(true);
					ano2.setEnabled(true);
					mes2.setEnabled(true);
					dia2.setEnabled(true);
				}
			}
		});
		chckbxBuquedaPorFecha.setFont(new Font("Tahoma", Font.BOLD, 14));
		chckbxBuquedaPorFecha.setBounds(15, 12, 185, 29);
		panel.add(chckbxBuquedaPorFecha);
		
		
		chckbxBuquedaPorItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (chckbxBuquedaPorItem.isSelected()) {
					chckbxBuquedaPorFecha.setSelected(false);
					buscarpor1.setEnabled(true);
					Ano.setEnabled(false);
					mes.setEnabled(false);
					dia.setEnabled(false);
					ano2.setEnabled(false);
					mes2.setEnabled(false);
					dia2.setEnabled(false);
					
				}
			}
		});
		chckbxBuquedaPorItem.setFont(new Font("Tahoma", Font.BOLD, 14));
		chckbxBuquedaPorItem.setBounds(217, 12, 185, 29);
		panel.add(chckbxBuquedaPorItem);
		
		JLabel lblAo = new JLabel("A\u00F1o");
		lblAo.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblAo.setBounds(115, 56, 37, 20);
		panel.add(lblAo);
		
		Ano = new JTextField();
		Ano.setBounds(154, 53, 51, 26);
		panel.add(Ano);
		Ano.setColumns(10);
		Ano.setEnabled(false);
		
		JLabel lblMes = new JLabel("Mes");
		lblMes.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblMes.setBounds(217, 53, 37, 20);
		panel.add(lblMes);
		
		mes = new JTextField();
		mes.setColumns(10);
		mes.setBounds(250, 50, 56, 26);
		panel.add(mes);
		mes.setEnabled(false);
		
		JLabel lblDia = new JLabel("Dia");
		lblDia.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblDia.setBounds(316, 56, 37, 20);
		panel.add(lblDia);
		
		dia = new JTextField();
		dia.setColumns(10);
		dia.setBounds(342, 50, 55, 26);
		panel.add(dia);
		dia.setEnabled(false);
		
		
		JLabel lblNewLabel = new JLabel("Buscar por Item:");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setBounds(15, 131, 129, 20);
		panel.add(lblNewLabel);
		
		JLabel lblFechDesde = new JLabel("Fecha desde:");
		lblFechDesde.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblFechDesde.setBounds(15, 56, 95, 20);
		panel.add(lblFechDesde);
		
		JLabel label = new JLabel("Fecha hasta:");
		label.setFont(new Font("Tahoma", Font.BOLD, 14));
		label.setBounds(15, 95, 95, 20);
		panel.add(label);
		
		JLabel label_1 = new JLabel("A\u00F1o");
		label_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_1.setBounds(115, 95, 37, 20);
		panel.add(label_1);
		
		ano2 = new JTextField();
		ano2.setColumns(10);
		ano2.setBounds(154, 92, 51, 26);
		panel.add(ano2);
		ano2.setEnabled(false);
		
		JLabel label_2 = new JLabel("Mes");
		label_2.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_2.setBounds(217, 92, 37, 20);
		panel.add(label_2);
		
		mes2 = new JTextField();
		mes2.setColumns(10);
		mes2.setBounds(250, 89, 56, 26);
		panel.add(mes2);
		mes2.setEnabled(false);
		
		JLabel label_3 = new JLabel("Dia");
		label_3.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_3.setBounds(316, 95, 37, 20);
		panel.add(label_3);
		
		dia2 = new JTextField();
		dia2.setColumns(10);
		dia2.setBounds(342, 89, 55, 26);
		panel.add(dia2);
		dia2.setEnabled(false);
		
		buscarpor1 = new JTextField();
		buscarpor1.setBounds(137, 128, 146, 26);
		panel.add(buscarpor1);
		buscarpor1.setColumns(10);
		buscarpor1.setEnabled(false);
		
		JRadioButton rdbtnTodasLasTablas = new JRadioButton("Todas las tablas");
		rdbtnTodasLasTablas.setBounds(192, 158, 155, 29);
		panel.add(rdbtnTodasLasTablas);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(66, 164, 115, 26);
		panel.add(comboBox_1);
		
		JButton Button_buscar1 = new JButton("Buscar");
		Button_buscar1.setBounds(353, 158, 104, 29);
		panel.add(Button_buscar1);	
	}
	
	
	public void control2() {
		
		JPanel panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBounds(15, 239, 462, 189);
		contentPane.add(panel_1);
		
		JCheckBox checkBox_fecha = new JCheckBox("Buqueda por fecha");
		JCheckBox checkBox_1_item = new JCheckBox("Buqueda por item");
		
		JLabel label_4 = new JLabel("Tabla:");
		label_4.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_4.setBounds(15, 167, 69, 20);
		panel_1.add(label_4);
		
		
		checkBox_fecha.addActionListener(new ActionListener() {/////////////////////////////////////////////////////////////77
			public void actionPerformed(ActionEvent e) {
				if (checkBox_fecha.isSelected()) {
					checkBox_1_item.setSelected(false);
					ano22.setEnabled(true);
					mes22.setEnabled(true);
					dia22.setEnabled(true);
					ano23.setEnabled(true);
					mes23.setEnabled(true);
					dia23.setEnabled(true);
					buscarpor2.setEnabled(false);
				}
				
			}
		});
		checkBox_fecha.setFont(new Font("Tahoma", Font.BOLD, 14));
		checkBox_fecha.setBounds(15, 12, 185, 29);
		panel_1.add(checkBox_fecha);
		
		
		checkBox_1_item.addActionListener(new ActionListener() {///////////////////////////////////////////////////////////////////7
			public void actionPerformed(ActionEvent e) {
				if (checkBox_1_item.isSelected()) {
					checkBox_fecha.setSelected(false);
					buscarpor2.setEnabled(true);
					ano22.setEnabled(false);
					mes22.setEnabled(false);
					dia22.setEnabled(false);
					ano23.setEnabled(false);
					mes23.setEnabled(false);
					dia23.setEnabled(false);
				}
				
			}
		});
		checkBox_1_item.setFont(new Font("Tahoma", Font.BOLD, 14));
		checkBox_1_item.setBounds(217, 12, 185, 29);
		panel_1.add(checkBox_1_item);
		
		JLabel label_5 = new JLabel("A\u00F1o");
		label_5.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_5.setBounds(115, 56, 37, 20);
		panel_1.add(label_5);
		
		ano22 = new JTextField();
		ano22.setColumns(10);
		ano22.setBounds(154, 53, 51, 26);
		panel_1.add(ano22);
		
		JLabel label_6 = new JLabel("Mes");
		label_6.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_6.setBounds(217, 53, 37, 20);
		panel_1.add(label_6);
		
		mes22 = new JTextField();
		mes22.setColumns(10);
		mes22.setBounds(250, 50, 56, 26);
		panel_1.add(mes22);
		
		JLabel label_7 = new JLabel("Dia");
		label_7.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_7.setBounds(316, 56, 37, 20);
		panel_1.add(label_7);
		
		dia22 = new JTextField();
		dia22.setColumns(10);
		dia22.setBounds(342, 50, 55, 26);
		panel_1.add(dia22);
		
		JLabel label_8 = new JLabel("Buscar por Item:");
		label_8.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_8.setBounds(15, 131, 129, 20);
		panel_1.add(label_8);
		
		JLabel label_9 = new JLabel("Fecha desde:");
		label_9.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_9.setBounds(15, 56, 95, 20);
		panel_1.add(label_9);
		
		JLabel label_10 = new JLabel("Fecha hasta:");
		label_10.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_10.setBounds(15, 95, 95, 20);
		panel_1.add(label_10);
		
		JLabel label_11 = new JLabel("A\u00F1o");
		label_11.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_11.setBounds(115, 95, 37, 20);
		panel_1.add(label_11);
		
		ano23 = new JTextField();
		ano23.setColumns(10);
		ano23.setBounds(154, 92, 51, 26);
		panel_1.add(ano23);
		
		JLabel label_12 = new JLabel("Mes");
		label_12.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_12.setBounds(217, 92, 37, 20);
		panel_1.add(label_12);
		
		mes23 = new JTextField();
		mes23.setColumns(10);
		mes23.setBounds(250, 89, 56, 26);
		panel_1.add(mes23);
		
		JLabel label_13 = new JLabel("Dia");
		label_13.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_13.setBounds(316, 95, 37, 20);
		panel_1.add(label_13);
		
		dia23 = new JTextField();
		dia23.setColumns(10);
		dia23.setBounds(342, 89, 55, 26);
		panel_1.add(dia23);
		
		buscarpor2 = new JTextField();
		buscarpor2.setColumns(10);
		buscarpor2.setBounds(137, 128, 146, 26);
		panel_1.add(buscarpor2);
		buscarpor2.setEnabled(false);
		
		ano22.setEnabled(false);
		mes22.setEnabled(false);
		dia22.setEnabled(false);
		ano23.setEnabled(false);
		mes23.setEnabled(false);
		dia23.setEnabled(false);
		
		
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setBounds(66, 164, 115, 26);
		panel_1.add(comboBox_2);
		
		JButton button_buscar2 = new JButton("Buscar");
		button_buscar2.setBounds(353, 158, 104, 29);
		panel_1.add(button_buscar2);
	}
	
	
	
	public void control3() {
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(15, 431, 462, 189);
		contentPane.add(panel);
		
		JLabel label = new JLabel("Tabla:");
		label.setFont(new Font("Tahoma", Font.BOLD, 14));
		label.setBounds(15, 167, 69, 20);
		panel.add(label);
		
		JCheckBox checkBox_item3 = new JCheckBox("Buqueda por item");
		JCheckBox checkBox_fecha3 = new JCheckBox("Buqueda por fecha");
		
		checkBox_fecha3.addActionListener(new ActionListener() {//////////////////////////////////////////////////////////////////77
			public void actionPerformed(ActionEvent e) {
				
				if (checkBox_fecha3.isSelected()) {
					checkBox_item3.setSelected(false);
					ano33.setEnabled(true);
					mes33.setEnabled(true);
					dia33.setEnabled(true);
					ano32.setEnabled(true);
					mes32.setEnabled(true);
					dia32.setEnabled(true);
					buscarpor3.setEnabled(false);
				}
				
				
			}
		});
		checkBox_fecha3.setFont(new Font("Tahoma", Font.BOLD, 14));
		checkBox_fecha3.setBounds(15, 12, 185, 29);
		panel.add(checkBox_fecha3);
		
		
		checkBox_item3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {///////////////////////////////////////////7
				
				if (checkBox_item3.isSelected()) {
					checkBox_fecha3.setSelected(false);
					buscarpor3.setEnabled(true);
					ano33.setEnabled(false);
					mes33.setEnabled(false);
					dia33.setEnabled(false);
					ano32.setEnabled(false);
					mes32.setEnabled(false);
					dia32.setEnabled(false);
				}
					
				
			}
		});
		checkBox_item3.setFont(new Font("Tahoma", Font.BOLD, 14));
		checkBox_item3.setBounds(217, 12, 185, 29);
		panel.add(checkBox_item3);
		
		JLabel label_1 = new JLabel("A\u00F1o");
		label_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_1.setBounds(115, 56, 37, 20);
		panel.add(label_1);
		
		ano33 = new JTextField();
		ano33.setColumns(10);
		ano33.setBounds(154, 53, 51, 26);
		panel.add(ano33);
		
		JLabel label_2 = new JLabel("Mes");
		label_2.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_2.setBounds(217, 53, 37, 20);
		panel.add(label_2);
		
		mes33 = new JTextField();
		mes33.setColumns(10);
		mes33.setBounds(250, 50, 56, 26);
		panel.add(mes33);
		
		JLabel label_3 = new JLabel("Dia");
		label_3.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_3.setBounds(316, 56, 37, 20);
		panel.add(label_3);
		
		dia33 = new JTextField();
		dia33.setColumns(10);
		dia33.setBounds(342, 50, 55, 26);
		panel.add(dia33);
		
		JLabel label_14 = new JLabel("Buscar por Item:");
		label_14.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_14.setBounds(15, 131, 129, 20);
		panel.add(label_14);
		
		JLabel label_15 = new JLabel("Fecha desde:");
		label_15.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_15.setBounds(15, 56, 95, 20);
		panel.add(label_15);
		
		JLabel label_16 = new JLabel("Fecha hasta:");
		label_16.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_16.setBounds(15, 95, 95, 20);
		panel.add(label_16);
		
		JLabel label_17 = new JLabel("A\u00F1o");
		label_17.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_17.setBounds(115, 95, 37, 20);
		panel.add(label_17);
		
		ano32 = new JTextField();
		ano32.setColumns(10);
		ano32.setBounds(154, 92, 51, 26);
		panel.add(ano32);
		
		JLabel label_18 = new JLabel("Mes");
		label_18.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_18.setBounds(217, 92, 37, 20);
		panel.add(label_18);
		
		mes32 = new JTextField();
		mes32.setColumns(10);
		mes32.setBounds(250, 89, 56, 26);
		panel.add(mes32);
		
		JLabel label_19 = new JLabel("Dia");
		label_19.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_19.setBounds(316, 95, 37, 20);
		panel.add(label_19);
		
		dia32 = new JTextField();
		dia32.setColumns(10);
		dia32.setBounds(342, 89, 55, 26);
		panel.add(dia32);
		
		buscarpor3 = new JTextField();
		buscarpor3.setColumns(10);
		buscarpor3.setBounds(137, 128, 146, 26);
		panel.add(buscarpor3);
		
		buscarpor3.setEnabled(false);
		
		ano33.setEnabled(false);
		mes33.setEnabled(false);
		dia33.setEnabled(false);
		ano32.setEnabled(false);
		mes32.setEnabled(false);
		dia32.setEnabled(false);
		
		
		
		
		JComboBox comboBox_3 = new JComboBox();
		comboBox_3.setBounds(66, 164, 115, 26);
		panel.add(comboBox_3);
		
		JButton button_buscar3 = new JButton("Buscar");
		button_buscar3.setBounds(353, 158, 104, 29);
		panel.add(button_buscar3);
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	public void tabla1() {
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(492, 36, 574, 176);
		contentPane.add(scrollPane);

		table = new JTable();
		scrollPane.setViewportView(table);
	}

	public void tabla2() {
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(492, 239, 574, 176);
		contentPane.add(scrollPane_1);

		table_1 = new JTable();
		scrollPane_1.setViewportView(table_1);
		
	}
	
	

	public void tabla3() {

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(492, 431, 574, 176);
		contentPane.add(scrollPane_2);

		table_2 = new JTable();
		scrollPane_2.setViewportView(table_2);
		
	}
}
