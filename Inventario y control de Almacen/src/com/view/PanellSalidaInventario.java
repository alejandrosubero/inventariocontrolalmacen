package com.view;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.table.DefaultTableModel;

import com.view.panel.AccionTabla;

public class PanellSalidaInventario extends JFrame{

	
	
	
	//private ActionListener accion;
	//private AccionTabla accionTabla;
	
	
	/*private JTextField codigotxt;
	private JTextField textField_cantidad;
	private JTextField textField_areaalmacen;
	private JTextField textField_areaseccion;
	private JTextField textField_nota;
	private JTextField buscar;
	
	
	private JLabel lblNewLabel_1;
	private JLabel lblCantidadProducto_1;
	private JLabel lblAreaDeAlmacenaje_1;
	private JLabel lblSeccinDeAlmacenaje_1;
	private JLabel notalabel;
	
	
	
	//botones 
	private JButton ingresarButton;
	private JButton EditarSalvarButton;
	private JButton editarButton;
	private JButton cancelarButton;
	private JButton Borrar_Button;
	private JTextField id_oculto;
	private JPanel panel_contenedor1;

	//private DefaultTableModel model_1;
	private JScrollPane scrollPane;
	private JTable table;
	private JPanel panel_1;
	private JTable table_1;
	private JTextField buscarInventario;
	private PanelIngreso ingresoProducto;
	private PanelInventario inventario;
	private JMenu mnFile;
	private JMenuItem mntmNewMenuItem;
	private JMenuItem mntmNewMenuItem_1;
	private JLabel lblCargaDeProducto;
	*
	*
	*/
	//private int idII;
	
	private static String rol;
	private static String UserName;
	
	
	private boolean editar = false;
	private boolean ingreso=false;
	
	private int cantidad;
	private DefaultTableModel model;
	private JPanel panel_2;
	
	private JLabel lblSalidoDeProductos;
	private JLabel lblNewLabel_2;
	private JLabel lblNota;
	private JLabel lblCliente;
	private JLabel lblCantidad;
	
	private JScrollPane scrollPane_2;
	private JTable table_2;

	private JPanel panel_3;
	private JPanel corregir_panel;
	
	private JTextField codigo_textField;
	private JTextField cantidad_textField;
	private JTextField cliente_textField;
	private JTextField nota_textField;
	private JTextField buscar_salida;
	private JTextField areaalmacen;
	private JTextField areaseccion;
	private JTextField id_Salida_oculto;

	
	private JButton salida_editar_Button;

	
	public PanellSalidaInventario(String user, String rol) {
		this.UserName=user;
		this.rol=rol;
	}
	
	
	
	public void salidaInventario(JTabbedPane tabbedPane) {
	
		
		JPanel salidaInventario = new JPanel();
		salidaInventario.addAncestorListener(new AncestorListener() {
			public void ancestorAdded(AncestorEvent event) {
				editar=false;
				ingreso=false;
				
			}
			public void ancestorMoved(AncestorEvent event) {
			}
			public void ancestorRemoved(AncestorEvent event) {
			}
		});
		
		tabbedPane.addTab("Salida de Inventario", null, salidaInventario, null);
		
		panel_2 = new JPanel();
		
		scrollPane_2 = new JScrollPane();
		
		table_2 = new JTable(model);
		table_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				
				if (!editar) {
					int seleccion2 =table_2.rowAtPoint(evt.getPoint());
					id_Salida_oculto.setText(String.valueOf(table_2.getValueAt(seleccion2, 0)));
					codigo_textField.setText(String.valueOf(table_2.getValueAt(seleccion2, 1)));
					areaalmacen.setText(String.valueOf(table_2.getValueAt(seleccion2, 3)));
					areaseccion.setText(String.valueOf(table_2.getValueAt(seleccion2, 4)));
					nota_textField.setText(String.valueOf(table_2.getValueAt(seleccion2, 7)));
					
				}else if (editar) {
					
					int seleccion3 =table_2.rowAtPoint(evt.getPoint());
					id_Salida_oculto.setText(String.valueOf(table_2.getValueAt(seleccion3, 0)));
					codigo_textField.setText(String.valueOf(table_2.getValueAt(seleccion3, 1)));
					cantidad_textField.setText(String.valueOf(table_2.getValueAt(seleccion3, 2)));
					areaalmacen.setText(String.valueOf(table_2.getValueAt(seleccion3, 3)));
					areaseccion.setText(String.valueOf(table_2.getValueAt(seleccion3, 4)));
					cliente_textField.setText(String.valueOf(table_2.getValueAt(seleccion3, 7)));
					nota_textField.setText(String.valueOf(table_2.getValueAt(seleccion3, 6)));
				}
				
			}
		});
		scrollPane_2.setViewportView(table_2);
		
		lblNewLabel_2 = new JLabel("Codigo del Producto");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		id_Salida_oculto = new JTextField();
		id_Salida_oculto.setColumns(10);
		id_Salida_oculto.setVisible(false);

		
		codigo_textField = new JTextField();
		codigo_textField.setColumns(10);
		
		lblCantidad = new JLabel("Cantidad");
		lblCantidad.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		
		cantidad_textField = new JTextField();
		cantidad_textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				ingreso=true;
			}
			@Override
			public void keyTyped(KeyEvent even) {
				char c = even.getKeyChar();
				if (!(Character.isDigit(c) || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
				JOptionPane.showMessageDialog(null,"No puede ingresar letras!!!","Ventana Error Datos",JOptionPane.ERROR_MESSAGE);
				even.consume();
				}
			}
		});
	
		
		cantidad_textField.setColumns(10);
		
		lblNota = new JLabel("Nota");
		lblNota.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		lblCliente = new JLabel("Cliente");
		lblCliente.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		cliente_textField = new JTextField();
		cliente_textField.setColumns(10);
		
		panel_3 = new JPanel();
		
		
		nota_textField = new JTextField();
		nota_textField.setColumns(10);
		
		areaalmacen = new JTextField();
		areaalmacen.setColumns(10);
		areaalmacen.setVisible(false);
		
		areaseccion = new JTextField();
		areaseccion.setVisible(false);
		areaseccion.setColumns(10);
		
		
		buscar_salida = new JTextField();
		buscar_salida.setColumns(10);
		
		lblSalidoDeProductos = new JLabel("SALIDA DE PRODUCTOS");
		lblSalidoDeProductos.setFont(new Font("Tahoma", Font.BOLD, 20));
		
		
		
		
		GroupLayout gl_salidaInventario = new GroupLayout(salidaInventario);
		gl_salidaInventario.setHorizontalGroup(
			gl_salidaInventario.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_salidaInventario.createSequentialGroup()
					.addGap(406)
					.addComponent(lblSalidoDeProductos, GroupLayout.PREFERRED_SIZE, 259, GroupLayout.PREFERRED_SIZE))
				.addGroup(Alignment.TRAILING, gl_salidaInventario.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 1127, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_salidaInventario.setVerticalGroup(
			gl_salidaInventario.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_salidaInventario.createSequentialGroup()
					.addGap(34)
					.addComponent(lblSalidoDeProductos)
					.addGap(18)
					.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 481, Short.MAX_VALUE)
					.addGap(48))
		);
		
		salidaInventario.setLayout(gl_salidaInventario);
		
		Botones();
			
	}
	
	
	
	
	
	private void clearInput() {
		id_Salida_oculto.setText("");
		codigo_textField.setText("");
		cantidad_textField.setText("");
		areaalmacen.setText("");
		areaseccion.setText("");
		cliente_textField.setText("");
		nota_textField.setText("");
	}
	
	
	private void Botones() {
		
		JCheckBox chckbxRestarALa = new JCheckBox("Restar a la Salida");
		JCheckBox sumarCheckBox = new JCheckBox("Sumar a la Salida");
		JButton Busqueda_Button_1 = new JButton("Buscar");
		
		Busqueda_Button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {///////////////////////////////////////////////////////////////////////////
				
				editar=false;
				corregir_panel.setVisible(false);
				AccionTabla accionTabla = new AccionTabla(model, buscar_salida.getText(), table_2);
				accionTabla.findTableOfType("inventario");
				
			}
		});
		
		
		JButton salida_Button = new JButton("Guardar");
		salida_Button.setBounds(198, 26, 97, 25);
		salida_Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String operacion="";
				
				if (ingreso && editar) {
					if(chckbxRestarALa.isSelected() || sumarCheckBox.isSelected() ) {
						
						if (chckbxRestarALa.isSelected()) {
							operacion="resta";
						}else if (sumarCheckBox.isSelected()) {
							operacion="suma";
						}
						
						AccionTabla accionTabla = new AccionTabla(model, buscar_salida.getText(), table_2
								,codigo_textField.getText(), areaalmacen.getText(), areaseccion.getText()
								,nota_textField.getText(), UserName, rol);
										
						cantidad = Integer.parseInt(cantidad_textField.getText());
						
						accionTabla.corregirSalida(cantidad, cliente_textField.getText(), operacion);			
						ingreso = false;
						editar =false;
						clearInput();
						corregir_panel.setVisible(false);
						salida_Button.setText("Guardar");
						
					}else {
						JOptionPane.showMessageDialog(null, "Para editar la salida debe de seleccionar si le resta o le suma al inventario");
					}
					
				}else if(ingreso) {
										
					AccionTabla accionTabla = new AccionTabla(model, buscar_salida.getText(), table_2
							,codigo_textField.getText(), areaalmacen.getText(), areaseccion.getText()
							,nota_textField.getText(), UserName, rol);
					
					cantidad = Integer.parseInt(cantidad_textField.getText());
					
					accionTabla.updateSalida("salida", cantidad, cliente_textField.getText());

					ingreso = false;
					editar =false;
					salida_Button.setText("Guardar");
					clearInput();

				}
			}
		});
		
		
		panel_3.setLayout(null);
		panel_3.add(salida_Button);
		
		salida_editar_Button = new JButton("Corregir");
		salida_editar_Button.setBounds(81, 26, 97, 25);
		salida_editar_Button.addActionListener(new ActionListener() {////////////////////////////***********************************************************////////////////////////
			public void actionPerformed(ActionEvent e) {
				editar=true;
				
				if (editar) {
					AccionTabla accionTabla = new AccionTabla(model, buscar_salida.getText(), table_2);
					accionTabla.findInventarioSalida("salida");
					corregir_panel.setVisible(true);
					salida_Button.setText("Salvar");
					
				}
			}
		});
		
		panel_3.add(salida_editar_Button);
		
		
		corregir_panel = new JPanel();
		corregir_panel.setBounds(12, 67, 339, 109);
		panel_3.add(corregir_panel);
		corregir_panel.setLayout(null);
		corregir_panel.setVisible(false);
		
		JLabel corregir_tituloLabel_3 = new JLabel("Acciones a Realizar");
		corregir_tituloLabel_3.setBounds(99, 13, 132, 16);
		corregir_panel.add(corregir_tituloLabel_3);
		
		
		sumarCheckBox.setBounds(8, 38, 139, 25);
		corregir_panel.add(sumarCheckBox);
		sumarCheckBox.addActionListener(new ActionListener() {///////////////////////////////***********************************************///////////////////////////////////////////
			
			public void actionPerformed(ActionEvent e) {
				if (sumarCheckBox.isSelected()) {
					chckbxRestarALa.setSelected(false);
				}

			}
		});
		
		chckbxRestarALa.setBounds(181, 38, 139, 25);
		corregir_panel.add(chckbxRestarALa);
		chckbxRestarALa.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				if ( chckbxRestarALa.isSelected()) {
					sumarCheckBox.setSelected(false);
				}	
			}
		});

		JButton cancelar_Button = new JButton("Cancelar");
		cancelar_Button.addActionListener(new ActionListener() {/////////////////////////*************************************************//////////////////////////////////////////
			public void actionPerformed(ActionEvent e) {
				corregir_panel.setVisible(false);
				editar=false;
				ingreso=false;
				salida_Button.setText("Guardar");
			}
		});
		cancelar_Button.setBounds(113, 72, 97, 25);
		corregir_panel.add(cancelar_Button);
		
		GroupLayoutSalidaInventarioBotones(Busqueda_Button_1);
		
	}
	

	public void GroupLayoutSalidaInventarioBotones(JButton Busqueda_Button_1) {
		
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(12)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addComponent(id_Salida_oculto, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
							.addGap(5)
							.addComponent(Busqueda_Button_1, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
							.addGap(30)
							.addComponent(buscar_salida, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(12)
							.addComponent(areaalmacen, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
							.addGap(65)
							.addComponent(areaseccion, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(25)
							.addComponent(lblNewLabel_2, GroupLayout.PREFERRED_SIZE, 156, GroupLayout.PREFERRED_SIZE)
							.addGap(12)
							.addComponent(codigo_textField, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(25)
							.addComponent(lblCantidad, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
							.addGap(75)
							.addComponent(cantidad_textField, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(25)
							.addComponent(lblCliente, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
							.addGap(75)
							.addComponent(cliente_textField, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(25)
							.addComponent(lblNota, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
							.addGap(75)
							.addComponent(nota_textField, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
						.addGroup(Alignment.TRAILING, gl_panel_2.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 372, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(scrollPane_2, GroupLayout.DEFAULT_SIZE, 710, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(23)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(1)
							.addComponent(id_Salida_oculto, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
						.addComponent(Busqueda_Button_1, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(4)
							.addComponent(buscar_salida, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)))
					.addGap(13)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(3)
							.addComponent(areaalmacen, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
						.addComponent(areaseccion, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
					.addGap(14)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(1)
							.addComponent(lblNewLabel_2, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
						.addComponent(codigo_textField, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
					.addGap(27)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(1)
							.addComponent(lblCantidad, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
						.addComponent(cantidad_textField, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
					.addGap(23)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(1)
							.addComponent(lblCliente, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
						.addComponent(cliente_textField, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
					.addGap(27)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(1)
							.addComponent(lblNota, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
						.addComponent(nota_textField, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
					.addGap(13)
					.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 189, GroupLayout.PREFERRED_SIZE)
					.addGap(13))
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(27)
					.addComponent(scrollPane_2, GroupLayout.DEFAULT_SIZE, 428, Short.MAX_VALUE)
					.addGap(26))
		);
		panel_2.setLayout(gl_panel_2);	
		
	}

	
	
}
