package com.view.panel;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import com.entidades.Inventario;
import com.entidades.User;
import com.tool.seguridad.ImplementaConexionUsuario;

public class AccionTabla {

	private Accion conexionAccion;
	private DefaultTableModel modelo;
	private String buscapor;
	private JTable table;
	private String codigo;
	private String areaalmacen;
	private String areaseccion;
	private String nota;
	private String usuario;
	private String rol;
	// private String databaseName;
	public TableCellRenderer centerAlinght = new CentrarLasColumnas();
	
	
	//constructor de la clace aciontala
	public AccionTabla(DefaultTableModel modelo, String textField, JTable table, String codigotxt,
			String textField_areaalmacen, String textField_areaseccion, String textField_nota, String usuario, String rol) {	
		
		
		super();
		
		this.conexionAccion=new Accion();
		
		this.modelo = modelo;
		this.buscapor = textField;
		this.table = table;
		
		this.usuario= usuario;
		this.rol=rol;
		
		this.codigo = codigotxt;
	
		this.areaalmacen = textField_areaalmacen;
		this.areaseccion = textField_areaseccion;
		this.nota = textField_nota;
		
	}

	
	
	public AccionTabla() {
		this.conexionAccion=new Accion();
	}
	

	
	public AccionTabla(DefaultTableModel modelo, String textField, JTable table) {	
		super();
		this.conexionAccion=new Accion();
		this.modelo = modelo;
		this.buscapor = textField;
		this.table = table;
	}
	
	//int newcantidad, String cliente, String operacion
	public void updateSalida(String nameOfTable, int cantidadSalida, String cliente) {	

		int id_inventario=0;
		int cantidad_inventario=0;
		int cantidad=0;
		
	for (Inventario inventario : conexionAccion.ListaTablasAlmacenSalidaEditar(codigo, areaalmacen, areaseccion) ) {
			id_inventario= inventario.getId();
			cantidad_inventario= inventario.getCantidadProducto();
		}
		
		conexionAccion.addinventariototalSalida(codigo, cantidadSalida, areaalmacen, areaseccion, nota, cliente, usuario, rol);
		cantidad = cantidad_inventario - cantidadSalida;

		conexionAccion.inventarioUpdateSalida(cantidad, id_inventario);
		JOptionPane.showMessageDialog(null, "Se ejecuto la salida del producto");
		findInventario();
		
	}
	
	
	
	
	public void updateInventario(String nameOfTable, int cantidad, JTable table) {
		conexionAccion.addProducto(codigo, cantidad, areaalmacen, areaseccion, nota, usuario, rol);
		JOptionPane.showMessageDialog(null, "Ingresado");
		//eliminarTodaLaTabla();
		findTableOfType(nameOfTable);
	}
	
	
	public void findTableOfType(String nameOfTable) {
		
		if (nameOfTable.contentEquals("inventario")) {
			findInventario();
			
		}else if (nameOfTable.contentEquals("editado")) {
			findInventarioIngresoEditado(nameOfTable);
			
		}else if(nameOfTable.contentEquals("salida")){
			findInventarioSalida(nameOfTable);
		}else {
			findInventarioIngresoEditado(nameOfTable);
	}
	}
	
	
	
	public void findInventario() {// este metodo agrega a la tabla el model extraido de la base de datos
		
		String[] titulos = { "Id", "Codigo producto", "Cantidad", "Almacen Area", "Almacen Seccion",
				" Fecha del ultimoingreso", "Fecha de la ultima Salida", "Nota" };
		
		modelo = new DefaultTableModel(titulos, 0);
		modelo.setColumnIdentifiers(titulos);
		ArrayList<Inventario> inventarios = conexionAccion.inventarioLista(buscapor);
		
		llenadoInventariTabla(inventarios);
		
	}

	
	
	
	
	public void findInventarioporfecha(String fechaDesde, String fechaHasta, String nameOfTable) {// este metodo agrega a la tabla el model extraido de la base de datos
		
		ArrayList<Inventario> inventarios = conexionAccion.inventarioListaporfechas(buscapor, fechaDesde, fechaHasta,nameOfTable);
		
	if (nameOfTable.equals("inventario")) {
			
		String[] titulos = { "Id", "Codigo producto", "Cantidad", "Almacen Area", "Almacen Seccion",
					" Fecha del ultimoingreso", "Fecha de la ultima Salida", "Nota"};
		modelo = new DefaultTableModel(titulos, 0);
		modelo.setColumnIdentifiers(titulos);
			
		llenadoInventariTabla(inventarios);
		
		}else if (nameOfTable.equals("salida")) {
			 
			String[] titulos = { "Id", "Codigo producto", "Cantidad que Salio", "Area Almacen ", "Secci�n Almacen ",
						"Fecha de salida", "Nota", "Cliente", "Usuario"};
			
			modelo = new DefaultTableModel(titulos, 0);
			modelo.setColumnIdentifiers(titulos);
			llenadofindInventarioSalida( inventarios);
			
		}else if (nameOfTable.equals("ingreso")) {
			
			String[] titulos = {"Id", "Codigo producto", "Cantidad Recibida", "Almacen Area", "Almacen Seccion",
						"Fecha del ultimoingreso", "Nota", "Usuario"};		
			modelo = new DefaultTableModel(titulos, 0);
			modelo.setColumnIdentifiers(titulos);
			llenadofindInventarioIngresoEditado( inventarios);
			
		}else if (nameOfTable.equals("editado")) {
			
			String[] titulos = { "Id", "Codigo producto", "Cantidad", "Almacen Area", "Almacen Seccion",
					" Fecha del ultimoingreso", "Fecha de la ultima Salida", "Nota"};
			
			modelo = new DefaultTableModel(titulos, 0);
			modelo.setColumnIdentifiers(titulos);
			llenadofindInventarioIngresoEditado( inventarios);
		}
		

		
		
	}


	
	public void llenadoInventariTabla(ArrayList<Inventario> inventarios) {
		
		Object[] row = new Object[8];

		for (int i = 0; i < inventarios.size(); i++) {
			row[0] = inventarios.get(i).getId();
			row[1] = inventarios.get(i).getCodigoProducto();
			row[2] = inventarios.get(i).getCantidadProducto();
			row[3] = inventarios.get(i).getAreaAlmacen();
			row[4] = inventarios.get(i).getSeccionAlmacen();
			row[5] = inventarios.get(i).getFechaUltimoIngreso();
			row[6] = inventarios.get(i).getFechaSalida();
			row[7] = inventarios.get(i).getNota();
			modelo.addRow(row);
		}
		table.setModel(modelo);
		table.getColumnModel().getColumn(0).setCellRenderer(centerAlinght);
		table.getColumnModel().getColumn(1).setCellRenderer(centerAlinght);
		table.getColumnModel().getColumn(2).setCellRenderer(centerAlinght);
		table.getColumnModel().getColumn(3).setCellRenderer(centerAlinght);
		table.getColumnModel().getColumn(4).setCellRenderer(centerAlinght);
		table.getColumnModel().getColumn(5).setCellRenderer(centerAlinght);
		table.getColumnModel().getColumn(6).setCellRenderer(centerAlinght);
		table.getColumnModel().getColumn(7).setCellRenderer(centerAlinght);

	}
	
	
	
	
	public void llenadofindInventarioIngresoEditado(ArrayList<Inventario> inventarios) {
		
		Object[] row = new Object[8];

		for (int i = 0; i < inventarios.size(); i++) {
			row[0] = inventarios.get(i).getId();
			row[1] = inventarios.get(i).getCodigoProducto();
			row[2] = inventarios.get(i).getCantidadRecibida();
			row[3] = inventarios.get(i).getAreaAlmacen();
			row[4] = inventarios.get(i).getSeccionAlmacen();
			row[5] = inventarios.get(i).getFechaUltimoIngreso();
			row[6] = inventarios.get(i).getNota();
			row[7] = inventarios.get(i).getUsuario();
			modelo.addRow(row);
		}
		table.setModel(modelo);
		table.getColumnModel().getColumn(0).setCellRenderer(centerAlinght);
		table.getColumnModel().getColumn(1).setCellRenderer(centerAlinght);
		table.getColumnModel().getColumn(2).setCellRenderer(centerAlinght);
		table.getColumnModel().getColumn(3).setCellRenderer(centerAlinght);
		table.getColumnModel().getColumn(4).setCellRenderer(centerAlinght);
		table.getColumnModel().getColumn(5).setCellRenderer(centerAlinght);
		table.getColumnModel().getColumn(6).setCellRenderer(centerAlinght);
		table.getColumnModel().getColumn(7).setCellRenderer(centerAlinght);
		
	}
	
	
	
	//Metodo pensado para auditoria y ver que se edita o agrega durante el ingreso de productos
	public void findInventarioIngresoEditado(String nameOfTable) {

		String[] titulos = { "Id", "Codigo producto", "Cantidad Recibida", "Almacen Area", "Almacen Seccion",
				"Fecha del ultimoingreso", "Nota", "Usuario"};
		modelo = new DefaultTableModel(titulos, 0);
		modelo.setColumnIdentifiers(titulos);      
		
		ArrayList<Inventario> inventarios = conexionAccion.ListaTablasAlmacen(buscapor,nameOfTable);
		llenadofindInventarioIngresoEditado( inventarios);
		
	}
	
	public void llenadofindInventarioSalida(ArrayList<Inventario> inventarios) {
		
		Object[] row = new Object[9];

		for (int i = 0; i < inventarios.size(); i++) {	
			row[0] = inventarios.get(i).getId();
			row[1] = inventarios.get(i).getCodigoProducto();
			row[2] = inventarios.get(i).getCantidadSalida();
			row[3] = inventarios.get(i).getAreaAlmacen();
			row[4] = inventarios.get(i).getSeccionAlmacen();
			row[5] = inventarios.get(i).getFechaSalida();
			row[6] = inventarios.get(i).getCliente();
			row[7] = inventarios.get(i).getNota();
			row[8] = inventarios.get(i).getUsuario();
			modelo.addRow(row);
		}
		table.setModel(modelo);
		
		table.getColumnModel().getColumn(0).setCellRenderer(centerAlinght);
		table.getColumnModel().getColumn(1).setCellRenderer(centerAlinght);
		table.getColumnModel().getColumn(2).setCellRenderer(centerAlinght);
		table.getColumnModel().getColumn(3).setCellRenderer(centerAlinght);
		table.getColumnModel().getColumn(4).setCellRenderer(centerAlinght);
		table.getColumnModel().getColumn(5).setCellRenderer(centerAlinght);
		table.getColumnModel().getColumn(6).setCellRenderer(centerAlinght);
		table.getColumnModel().getColumn(7).setCellRenderer(centerAlinght);
		
	}
	
	
	
	
	
	
	
	
	public void findInventarioSalida(String nameOfTable) {
		
		String[] titulos = { "Id", "Codigo producto", "Cantidad que Salio", "Area Almacen ", "Secci�n Almacen ",
				"Fecha de salida", "Nota", "Cliente", "Usuario"};
		
		modelo = new DefaultTableModel(titulos, 0);
		modelo.setColumnIdentifiers(titulos); 
		
		ArrayList<Inventario> inventarios = conexionAccion.ListaTablasAlmacen(buscapor,nameOfTable);
		llenadofindInventarioSalida( inventarios);
	}
	
	
	
	
	public Map <String, Object[]> empinfoMapInventario( String nameOfTable, String buscar) {//***************++++++++++++
		
			
		
		
		ImplementaConexionUsuario conex = new ImplementaConexionUsuario();
		Map <String, Object[]> empinfo = new TreeMap < String, Object[] >();
		int contador =0;
		
		if (nameOfTable.equals("inventario")) {
			
			empinfo.put( "1", new Object[] { "Id", "Codigo producto", "Cantidad", "Almacen Area", "Almacen Seccion",
					" Fecha del ultimoingreso", "Fecha de la ultima Salida", "Nota"});
			
		}else if (nameOfTable.equals("salida")) {
			 
			empinfo.put( "1", new Object[] { "Id", "Codigo producto", "Cantidad que Salio", "Area Almacen ", "Secci�n Almacen ",
						"Fecha de salida", "Nota", "Cliente", "Usuario"});
				
		}else if (nameOfTable.equals("ingreso")) {
			
			 empinfo.put( "1", new Object[] {"Id", "Codigo producto", "Cantidad Recibida", "Almacen Area", "Almacen Seccion",
						"Fecha del ultimoingreso", "Nota", "Usuario"});			 
		
		}else if (nameOfTable.equals("editado")) {
			
			empinfo.put( "1", new Object[] { "Id", "Codigo producto", "Cantidad", "Almacen Area", "Almacen Seccion",
					" Fecha del ultimoingreso", "Fecha de la ultima Salida", "Nota"});
			
		}
	
		
		ArrayList<Inventario> inventarios = conexionAccion.ListaTablasAlmacen(buscar,nameOfTable);
		
		for (int i = 0; i < inventarios.size(); i++) {
			
			contador =2+i;
			String value =Integer.toString(contador);
			
			if (nameOfTable.equals("inventario")) {
				
				empinfo.put( value, new Object[] { inventarios.get(i).getId()
						, inventarios.get(i).getCodigoProducto(), inventarios.get(i).getCantidadProducto()
						, inventarios.get(i).getAreaAlmacen(), inventarios.get(i).getSeccionAlmacen(),
						inventarios.get(i).getFechaUltimoIngreso(), inventarios.get(i).getFechaSalida()
						, inventarios.get(i).getNota()});
				
			}else if (nameOfTable.equals("salida")) {
				
				empinfo.put( value, new Object[] { inventarios.get(i).getId()
						, inventarios.get(i).getCodigoProducto(), inventarios.get(i).getCantidadSalida()
						, inventarios.get(i).getAreaAlmacen(), inventarios.get(i).getSeccionAlmacen(),
						inventarios.get(i).getFechaSalida(), inventarios.get(i).getNota()
						, inventarios.get(i).getCliente(), inventarios.get(i).getUsuario()});
				
			}else if (nameOfTable.equals("editado")) {
				
				empinfo.put( value, new Object[] { inventarios.get(i).getId()
						, inventarios.get(i).getCodigoProducto(), inventarios.get(i).getCantidadRecibida()
						, inventarios.get(i).getAreaAlmacen(), inventarios.get(i).getSeccionAlmacen(),
						inventarios.get(i).getFechaUltimoIngreso(), inventarios.get(i).getNota()
						,inventarios.get(i).getUsuario()});
			
			}else if (nameOfTable.equals("ingreso")) {
				
				empinfo.put( value, new Object[] { inventarios.get(i).getId()
						, inventarios.get(i).getCodigoProducto(), inventarios.get(i).getCantidadRecibida()
						, inventarios.get(i).getAreaAlmacen(), inventarios.get(i).getSeccionAlmacen(),
						inventarios.get(i).getFechaUltimoIngreso(), inventarios.get(i).getNota()
						,inventarios.get(i).getUsuario()});
			}
		}
		return empinfo;
	}
	
	
	
	
	
	
	public void eliminarTodaLaTabla() {
	 conexionAccion.borrar(codigo,usuario, rol);
	}
	
	
	
	public void botonDeSalveEdition(String usuario, String rol, int cantidad, int id) {
		conexionAccion.salvarEdicion(codigo, usuario, rol);
		conexionAccion.inventarioUpdate(codigo, cantidad, areaalmacen, areaseccion, id);
		conexionAccion.salvarEdicion(codigo, usuario, rol);
	}
	
	
	
	
	public void corregirSalida(int newcantidad, String cliente, String operacion) {
		
		int id_inventario=0;
		int cantidad_inventario=0;
		int cantidad=0;
		
	for (Inventario inventario : conexionAccion.ListaTablasAlmacenSalidaEditar(codigo, areaalmacen, areaseccion) ) {
			id_inventario= inventario.getId();
			cantidad_inventario= inventario.getCantidadProducto();
		}
		
	if (operacion.equals("suma")) {
	cantidad = cantidad_inventario + newcantidad;
	
	}else if (operacion.equals("resta")) {
		conexionAccion.addinventariototalSalida(codigo, newcantidad, areaalmacen, areaseccion, nota, cliente, usuario, rol);	
		cantidad = cantidad_inventario - newcantidad;

	}
	
	conexionAccion.inventarioUpdateSalida(cantidad, id_inventario);
	conexionAccion.addinventariototalSalidaII(codigo, newcantidad, areaalmacen, areaseccion, nota, cliente, usuario, rol, operacion);
	JOptionPane.showMessageDialog(null, "Se ejecuto la Correcci�n");
	}
	
	
	
	
	

	
}
