package com.view.panel;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import com.entidades.User;
import com.tool.seguridad.ImplementaConexionUsuario;


public class ModelMetodosJTable {

	private DefaultTableModel modelo;
	private JTable table;
	private JTextField textField;
	
	private JTextField textField_id;
	private JTextField textField_User;
	private JTextField textField_Password;
	private JTextField textField_Rol;
	public TableCellRenderer centerAlinght = new CentrarLasColumnas();	
	
	public ModelMetodosJTable(DefaultTableModel model, JTable tabla, JTextField textField, JTextField textField_id, JTextField textField_User
			, JTextField textField_Password, JTextField textField_Rol) {
		
		this.modelo = model;
		this.table=tabla;
		this.textField=textField;
		this.textField_id=textField_id;
		this.textField_User =textField_User;
		this.textField_Password = textField_Password;
		this.textField_Rol=textField_Rol;
	}
	
	public ModelMetodosJTable() {}
	

	public void update() {			
     ImplementaConexionUsuario conex = new ImplementaConexionUsuario();
		ArrayList<User> users = conex.UserList(textField_User.getText());
			
			if (users.size()!=0) {
					JOptionPane.showMessageDialog(null, "Nombre de usuario en uso");
			}else if (users.size()==0) {
				addUser();
				JOptionPane.showMessageDialog(null, "usuario ingresado");
				clearBoxs();
				findUser();
			}
		}


	
	
	public Map <String, Object[]> empinfoMap( String vacio) {
		
		vacio="";
		ImplementaConexionUsuario conex = new ImplementaConexionUsuario();
		Map <String, Object[]> empinfo = new TreeMap < String, Object[] >();
	    empinfo.put( "1", new Object[] { "Id", "User", "Rol"});

		ArrayList<User> users = conex.UserList(vacio);
		int contador =0;
		for (int i = 0; i < users.size(); i++) {
			contador =2+i;
			String value =Integer.toString(contador);
			empinfo.put( value, new Object[] { users.get(i).getId()
					, users.get(i).getUser(), users.get(i).getRol()});
		}	
		return empinfo;
	}
	

	
	public void findUser() {

		String[] titulos = { "Id", "User", "Rol" };
		modelo = new DefaultTableModel(titulos, 0);
		modelo.setColumnIdentifiers(titulos);
		ImplementaConexionUsuario conex = new ImplementaConexionUsuario();
		ArrayList<User> users = conex.UserList(textField.getText());
		Object[] row = new Object[3];

		for (int i = 0; i < users.size(); i++) {
			row[0] = users.get(i).getId();
			row[1] = users.get(i).getUser();
			row[2] = users.get(i).getRol();

			modelo.addRow(row);
		}
		table.setModel(modelo);
		table.getColumnModel().getColumn(0).setCellRenderer(centerAlinght);
		table.getColumnModel().getColumn(1).setCellRenderer(centerAlinght);
		table.getColumnModel().getColumn(2).setCellRenderer(centerAlinght);
	}
	
	
	
	public void addUser( ) {
		String query = "INSERT INTO register (user,password,rol) values ('" + textField_User.getText() + "','"
				+ textField_Password.getText() + "','" + textField_Rol.getText() + "');";
		ImplementaConexionUsuario conectar = new ImplementaConexionUsuario();
		conectar.addAndUse(query);
	}
	
	
	
	public void userUpdate() {
	String query = "UPDATE register SET `password` = '"+textField_Password.getText()
	+"' WHERE `id` = "+textField_id.getText()+";";
	ImplementaConexionUsuario conectar = new ImplementaConexionUsuario();
	conectar.addAndUse(query);
	
	}
	
	
	private void borraRowOFdatabaseUser(int id) {
		String query = "DELETE FROM  register WHERE (`id` = '" + id + "');";
		ImplementaConexionUsuario conectar = new ImplementaConexionUsuario();
		conectar.addAndUse(query);
	}

	
	public void borrarUser(int id) {
		borraRowOFdatabaseUser(id);
		JOptionPane.showMessageDialog(null, "Usuario Borrado");
		clearBoxs();
		findUser();
	}
	
	
	public void clearBoxs() {
		textField_id.setText("");
		textField_User.setText("");
		textField_Password.setText("");
		textField_Rol.setText("");
	}

	
}
