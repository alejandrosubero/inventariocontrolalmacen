package com.view.panel;

import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;
import javax.swing.table.DefaultTableModel;

public class PanelAuditoriaComponentes extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3051999060160734065L;

	private JTable table_Prueba;
	private DefaultTableModel model;

	private JTextField textField_ano_prueba;
	private JTextField textField_mes_prueba;
	private JTextField textField_dia_prueba;
	private JTextField textField_ano2_prueba;
	private JTextField textField__mes2_prueba;
	private JTextField textField_dia2_prueba;
	private JTextField buscarpor_prueba;
	private JPanel panel_prueba;

	private JPanel super_panel;
	private String variable;
	private boolean check;

	public PanelAuditoriaComponentes(JPanel panel_prueba) {
		super();
		this.panel_prueba = panel_prueba;
		getContentPane().setLayout(null);
	}

	public void panelDePrueba() {

		super_panel = new JPanel();
		super_panel.setBounds(12, 13, 1860, 195);
		// contentPane.add(super_panel);
		panel_prueba.add(super_panel);
		/////////////////////////////////////////////////////////////////////////////////////

		JScrollPane scrollPane_prueba = new JScrollPane();
		table_Prueba = new JTable(model);
		scrollPane_prueba.setViewportView(table_Prueba);

		///////////////////////////////////////////////////////////////////////////////

		JPanel panel = new JPanel();
		panel.setLayout(null);

		JCheckBox checkBox_item_prueba = new JCheckBox("Buqueda por item");
		JCheckBox checkBoxfecha_prueba = new JCheckBox("Buqueda por fecha");
		JButton button_buscar_prueba = new JButton("Buscar");
		
		String[] tablas = { "inventario", "salida", "editado", "ingreso" };
		JComboBox comboBox_prueba = new JComboBox(tablas);
		
		
		checkBoxfecha_prueba.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {///////////////////////////////////////////////////
				if (checkBoxfecha_prueba.isSelected()) {
					checkBox_item_prueba.setSelected(false);
					buscarpor_prueba.setEnabled(false);
					textField_ano_prueba.setEnabled(true);
					textField_mes_prueba.setEnabled(true);
					textField_dia_prueba.setEnabled(true);
					textField_ano2_prueba.setEnabled(true);
					textField__mes2_prueba.setEnabled(true);
					textField_dia2_prueba.setEnabled(true);
					comboBox_prueba.setEnabled(true);
					

					LocalDate currentDate = LocalDate.now();
					String anodede = "" + currentDate.getYear() + "";
					textField_ano_prueba.setText(anodede);
					textField_ano2_prueba.setText(anodede);
					button_buscar_prueba.setEnabled(true);

				}
			}
		});
		checkBoxfecha_prueba.setBounds(73, 5, 157, 29);
		checkBoxfecha_prueba.setFont(new Font("Tahoma", Font.BOLD, 14));
		panel.add(checkBoxfecha_prueba);

		checkBox_item_prueba.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {///////////////////////////////////////////////////
				if (checkBox_item_prueba.isSelected()) {
					checkBoxfecha_prueba.setSelected(false);
					buscarpor_prueba.setEnabled(true);
					textField_ano_prueba.setEnabled(false);
					textField_mes_prueba.setEnabled(false);
					textField_dia_prueba.setEnabled(false);
					textField_ano2_prueba.setEnabled(false);
					textField__mes2_prueba.setEnabled(false);
					textField_dia2_prueba.setEnabled(false);
					comboBox_prueba.setEnabled(true);
					button_buscar_prueba.setEnabled(true);
					
				}
			}
		});
		checkBox_item_prueba.setBounds(235, 5, 163, 29);
		checkBox_item_prueba.setFont(new Font("Tahoma", Font.BOLD, 14));
		panel.add(checkBox_item_prueba);

		JLabel label = new JLabel("Fecha desde:");
		label.setFont(new Font("Tahoma", Font.BOLD, 14));
		label.setBounds(15, 49, 95, 20);
		panel.add(label);

		JLabel label_1 = new JLabel("A\u00F1o");
		label_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_1.setBounds(115, 49, 37, 20);
		panel.add(label_1);

		textField_ano_prueba = new JTextField();
		textField_ano_prueba.setEnabled(false);
		textField_ano_prueba.setColumns(10);
		textField_ano_prueba.setBounds(154, 46, 51, 26);
		panel.add(textField_ano_prueba);

		JLabel label_2 = new JLabel("Mes");
		label_2.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_2.setBounds(217, 46, 37, 20);
		panel.add(label_2);

		textField_mes_prueba = new JTextField();
		textField_mes_prueba.setEnabled(false);
		textField_mes_prueba.setColumns(10);
		textField_mes_prueba.setBounds(250, 43, 56, 26);
		panel.add(textField_mes_prueba);

		JLabel label_3 = new JLabel("Dia");
		label_3.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_3.setBounds(316, 49, 37, 20);
		panel.add(label_3);

		textField_dia_prueba = new JTextField();
		textField_dia_prueba.setEnabled(false);
		textField_dia_prueba.setColumns(10);
		textField_dia_prueba.setBounds(342, 43, 55, 26);
		panel.add(textField_dia_prueba);

		JLabel label_4 = new JLabel("Fecha hasta:");
		label_4.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_4.setBounds(15, 88, 95, 20);
		panel.add(label_4);

		JLabel label_5 = new JLabel("A\u00F1o");
		label_5.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_5.setBounds(115, 88, 37, 20);
		panel.add(label_5);

		textField_ano2_prueba = new JTextField();
		textField_ano2_prueba.setEnabled(false);
		textField_ano2_prueba.setColumns(10);
		textField_ano2_prueba.setBounds(154, 85, 51, 26);
		panel.add(textField_ano2_prueba);

		JLabel label_6 = new JLabel("Mes");
		label_6.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_6.setBounds(217, 85, 37, 20);
		panel.add(label_6);

		textField__mes2_prueba = new JTextField();
		textField__mes2_prueba.setEnabled(false);
		textField__mes2_prueba.setColumns(10);
		textField__mes2_prueba.setBounds(250, 82, 56, 26);
		panel.add(textField__mes2_prueba);

		JLabel label_7 = new JLabel("Dia");
		label_7.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_7.setBounds(316, 88, 37, 20);
		panel.add(label_7);

		textField_dia2_prueba = new JTextField();
		textField_dia2_prueba.setEnabled(false);
		textField_dia2_prueba.setColumns(10);
		textField_dia2_prueba.setBounds(342, 82, 55, 26);
		panel.add(textField_dia2_prueba);

		JLabel label_8 = new JLabel("Buscar por Item:");
		label_8.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_8.setBounds(15, 124, 129, 20);
		panel.add(label_8);

		buscarpor_prueba = new JTextField();
		buscarpor_prueba.setEnabled(false);
		buscarpor_prueba.setColumns(10);
		buscarpor_prueba.setBounds(137, 121, 146, 26);
		panel.add(buscarpor_prueba);

		
		comboBox_prueba.setBounds(66, 157, 115, 26);
		comboBox_prueba.setEnabled(false);
		panel.add(comboBox_prueba);

		comboBox_prueba.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				variable = comboBox_prueba.getSelectedItem().toString();
			}
		});

		JLabel label_9 = new JLabel("Tabla:");
		label_9.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_9.setBounds(15, 160, 69, 20);
		panel.add(label_9);

		
		button_buscar_prueba.setBounds(353, 151, 104, 29);
		button_buscar_prueba.setEnabled(false);
		panel.add(button_buscar_prueba);
		
		
		GroupLayout gl_super_panel = new GroupLayout(super_panel);
		gl_super_panel.setHorizontalGroup(gl_super_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_super_panel.createSequentialGroup().addGap(15)
						.addComponent(panel, GroupLayout.DEFAULT_SIZE, 564, Short.MAX_VALUE).addGap(27)
						.addComponent(scrollPane_prueba, GroupLayout.DEFAULT_SIZE, 977, Short.MAX_VALUE).addGap(277)));
		gl_super_panel
				.setVerticalGroup(gl_super_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_super_panel.createSequentialGroup().addGap(5)
								.addGroup(gl_super_panel.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_super_panel.createSequentialGroup()
												.addComponent(scrollPane_prueba, GroupLayout.DEFAULT_SIZE, 185,
														Short.MAX_VALUE)
												.addContainerGap())
										.addGroup(gl_super_panel.createSequentialGroup()
												.addComponent(panel, GroupLayout.DEFAULT_SIZE, 184, Short.MAX_VALUE)
												.addGap(6)))));
		super_panel.setLayout(gl_super_panel);

		
		button_buscar_prueba.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				AccionTabla accionTabla = new AccionTabla(model, buscarpor_prueba.getText(), table_Prueba);
				
				if (checkBoxfecha_prueba.isSelected()) {
					
					if (chequeo()) {
						String fechaDesde = textField_ano_prueba.getText() + "-" + textField_mes_prueba.getText() + "-"
								+ textField_dia_prueba.getText();
						String fechaHasta = textField_ano2_prueba.getText() + "-" + textField__mes2_prueba.getText() + "-"
								+ textField_dia2_prueba.getText();
						variable = comboBox_prueba.getSelectedItem().toString();
						accionTabla.findInventarioporfecha(fechaDesde, fechaHasta,variable);
					}
					
				} else if (checkBox_item_prueba.isSelected()) {

					variable = comboBox_prueba.getSelectedItem().toString();
					accionTabla.findTableOfType(variable);
				} else {
					variable = "";
					accionTabla.findTableOfType(variable);
				}
			}
		});
	}

	
	public boolean chequeo() {

		check = false;
		if (textField_ano_prueba.getText().length() >= 4 && textField_ano2_prueba.getText().length() >= 4) {
			if (textField_mes_prueba.getText().length() >= 2 && textField__mes2_prueba.getText().length() >= 2) {
				if (textField_dia_prueba.getText().length() >= 2 && textField_dia2_prueba.getText().length() >= 2) {
					check = true;
					return check;	
				}else {
					JOptionPane.showMessageDialog(null, "ingrese el dia en 2 digitos (06)");
					return check;
				}
			}else {
				JOptionPane.showMessageDialog(null, "ingrese el mes en 2 digitos (08)");
				return check;
			}
		}else {
			JOptionPane.showMessageDialog(null, "ingrese el a�o en 4 digitos (2019)");
			return check;
		}
	}

	
	
	
	public void componenteTabla() {

		JScrollPane scrollPane_prueba = new JScrollPane();
		scrollPane_prueba.setBounds(477, 16, 700, 176);
		panel_prueba.add(scrollPane_prueba);

		table_Prueba = new JTable(model);
		scrollPane_prueba.setViewportView(table_Prueba);
	}

	public void componentePanelControlPrueba() {

		JPanel panel = new JPanel();
		panel.setBounds(0, 16, 472, 183);
		panel_prueba.add(panel);
		panel.setLayout(null);
		
		String[] tablas = { "inventario", "salida", "editado", "ingreso" };
		
		JCheckBox checkBox_item_prueba = new JCheckBox("Buqueda por item");
		JCheckBox checkBoxfecha_prueba = new JCheckBox("Buqueda por fecha");
		JButton button_buscar_prueba = new JButton("Buscar");
		JComboBox comboBox_prueba = new JComboBox(tablas);
		
		checkBoxfecha_prueba.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {/////////////////////////////////////////////////// 77

				if (checkBoxfecha_prueba.isSelected()) {
					checkBox_item_prueba.setSelected(false);

					buscarpor_prueba.setEnabled(false);
					textField_ano_prueba.setEnabled(true);
					textField_mes_prueba.setEnabled(true);
					textField_dia_prueba.setEnabled(true);
					textField_ano2_prueba.setEnabled(true);
					textField__mes2_prueba.setEnabled(true);
					textField_dia2_prueba.setEnabled(true);
					button_buscar_prueba.setEnabled(true);
					comboBox_prueba.setEnabled(true);
				}
			}
		});
		checkBoxfecha_prueba.setBounds(73, 5, 157, 29);
		checkBoxfecha_prueba.setFont(new Font("Tahoma", Font.BOLD, 14));
		panel.add(checkBoxfecha_prueba);

		checkBox_item_prueba.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {//////////////////////////////////////////////////// 7777777

				if (checkBox_item_prueba.isSelected()) {
					checkBoxfecha_prueba.setSelected(false);
					buscarpor_prueba.setEnabled(true);
					textField_ano_prueba.setEnabled(false);
					textField_mes_prueba.setEnabled(false);
					textField_dia_prueba.setEnabled(false);
					textField_ano2_prueba.setEnabled(false);
					textField__mes2_prueba.setEnabled(false);
					textField_dia2_prueba.setEnabled(false);
					button_buscar_prueba.setEnabled(true);
					comboBox_prueba.setEnabled(true);
				}
			}
		});
		checkBox_item_prueba.setBounds(235, 5, 163, 29);
		checkBox_item_prueba.setFont(new Font("Tahoma", Font.BOLD, 14));
		panel.add(checkBox_item_prueba);

		JLabel label = new JLabel("Fecha desde:");
		label.setFont(new Font("Tahoma", Font.BOLD, 14));
		label.setBounds(15, 49, 95, 20);
		panel.add(label);

		JLabel label_1 = new JLabel("A\u00F1o");
		label_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_1.setBounds(115, 49, 37, 20);
		panel.add(label_1);

		textField_ano_prueba = new JTextField();
		textField_ano_prueba.setEnabled(false);
		textField_ano_prueba.setColumns(10);
		textField_ano_prueba.setBounds(154, 46, 51, 26);
		panel.add(textField_ano_prueba);

		JLabel label_2 = new JLabel("Mes");
		label_2.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_2.setBounds(217, 46, 37, 20);
		panel.add(label_2);

		textField_mes_prueba = new JTextField();
		textField_mes_prueba.setEnabled(false);
		textField_mes_prueba.setColumns(10);
		textField_mes_prueba.setBounds(250, 43, 56, 26);
		panel.add(textField_mes_prueba);

		JLabel label_3 = new JLabel("Dia");
		label_3.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_3.setBounds(316, 49, 37, 20);
		panel.add(label_3);

		textField_dia_prueba = new JTextField();
		textField_dia_prueba.setEnabled(false);
		textField_dia_prueba.setColumns(10);
		textField_dia_prueba.setBounds(342, 43, 55, 26);
		panel.add(textField_dia_prueba);

		JLabel label_4 = new JLabel("Fecha hasta:");
		label_4.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_4.setBounds(15, 88, 95, 20);
		panel.add(label_4);

		JLabel label_5 = new JLabel("A\u00F1o");
		label_5.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_5.setBounds(115, 88, 37, 20);
		panel.add(label_5);

		textField_ano2_prueba = new JTextField();
		textField_ano2_prueba.setEnabled(false);
		textField_ano2_prueba.setColumns(10);
		textField_ano2_prueba.setBounds(154, 85, 51, 26);
		panel.add(textField_ano2_prueba);

		JLabel label_6 = new JLabel("Mes");
		label_6.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_6.setBounds(217, 85, 37, 20);
		panel.add(label_6);

		textField__mes2_prueba = new JTextField();
		textField__mes2_prueba.setEnabled(false);
		textField__mes2_prueba.setColumns(10);
		textField__mes2_prueba.setBounds(250, 82, 56, 26);
		panel.add(textField__mes2_prueba);

		JLabel label_7 = new JLabel("Dia");
		label_7.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_7.setBounds(316, 88, 37, 20);
		panel.add(label_7);

		textField_dia2_prueba = new JTextField();
		textField_dia2_prueba.setEnabled(false);
		textField_dia2_prueba.setColumns(10);
		textField_dia2_prueba.setBounds(342, 82, 55, 26);
		panel.add(textField_dia2_prueba);

		JLabel label_8 = new JLabel("Buscar por Item:");
		label_8.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_8.setBounds(15, 124, 129, 20);
		panel.add(label_8);

		buscarpor_prueba = new JTextField();
		buscarpor_prueba.setEnabled(false);
		buscarpor_prueba.setColumns(10);
		buscarpor_prueba.setBounds(137, 121, 146, 26);
		panel.add(buscarpor_prueba);

		
		
		comboBox_prueba.setBounds(66, 157, 115, 26);
		comboBox_prueba.setEnabled(false);
		panel.add(comboBox_prueba);

		comboBox_prueba.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				variable = comboBox_prueba.getSelectedItem().toString();
			}
		});

		JLabel label_9 = new JLabel("Tabla:");
		label_9.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_9.setBounds(15, 160, 69, 20);
		panel.add(label_9);

		
		button_buscar_prueba.setBounds(353, 151, 104, 29);
		panel.add(button_buscar_prueba);
		button_buscar_prueba.setEnabled(false);
		button_buscar_prueba.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				AccionTabla accionTabla = new AccionTabla(model, buscarpor_prueba.getText(), table_Prueba);
				comboBox_prueba.getSelectedItem();

				accionTabla.findTableOfType(variable);

			}
		});
	}

}
