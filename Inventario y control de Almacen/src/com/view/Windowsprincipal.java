package com.view;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import com.view.panel.AccionTabla;
import com.view.panel.CentrarLasColumnas;

import excel.LibrosExcel;

import javax.swing.LayoutStyle.ComponentPlacement;

public class Windowsprincipal {

	private JFrame frame;
	private JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	
	private static String rol;
	private static String UserName;
	private int idII;
	
	private boolean editar = false;
	private boolean ingreso=false;
	
	private ActionListener accion;
	private AccionTabla accionTabla;
	
	
	private JTextField codigotxt;
	private JTextField textField_cantidad;
	private JTextField textField_areaalmacen;
	private JTextField textField_areaseccion;
	private JTextField textField_nota;
	private JTextField buscar;
	
	
	private JLabel lblNewLabel_1;
	private JLabel lblCantidadProducto_1;
	private JLabel lblAreaDeAlmacenaje_1;
	private JLabel lblSeccinDeAlmacenaje_1;
	private JLabel notalabel;
	
	
	
	//botones 
	private JButton ingresarButton;
	private JButton EditarSalvarButton;
	private JButton editarButton;
	private JButton cancelarButton;
	private JButton Borrar_Button;
	private JTextField id_oculto;
	
	
	private JPanel panel_contenedor1;

	
	private DefaultTableModel model;
	//private DefaultTableModel model_1;
	private JScrollPane scrollPane;
	private JTable table;
	private JPanel panel_1;
	private int cantidad;
	
	
	private JTable table_1;
	private JTextField buscarInventario;
	
	
	private JPanel panel_2;
	private JLabel lblSalidoDeProductos;
	private JScrollPane scrollPane_2;
	private JTable table_2;
	private JLabel lblNewLabel_2;
	private JTextField codigo_textField;
	private JLabel lblCantidad;
	private JTextField cantidad_textField;
	private JLabel lblNota;
	private JLabel lblCliente;
	private JTextField cliente_textField;
	
	
	private JPanel panel_3;
	private JPanel corregir_panel;
	
	private JTextField nota_textField;
	private JTextField buscar_salida;
	private JButton salida_editar_Button;
	private JTextField areaalmacen;
	private JTextField areaseccion;
	private JTextField id_Salida_oculto;
	private JMenu mnFile;
	private JMenuItem mntmNewMenuItem;
	private JMenuItem mntmNewMenuItem_1;
	
	private JLabel lblCargaDeProducto;
	
	
	private PanelIngreso ingresoProducto;
	private PanelInventario inventario;
	private PanellSalidaInventario inventarioSalida;
	
	
	JButton Exportar_Button;
	JPanel Inventario;
	private JMenuItem mntmCerrarYSalir;
	private JMenu exportarMenu;
	private JMenuItem mntmImprimirTabla;
	// private String databaseName;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Windowsprincipal window = new Windowsprincipal();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */

	
	public Windowsprincipal() {
		initialize();
		frame.setLocationRelativeTo(null);
	}

	public Windowsprincipal(String roll, String Name) {
		this.rol = roll;
		this.UserName = Name;
	}
	
	

//	public void iniciarVentana(String databaseName) {
//		this.databaseName = databaseName;
//		frame.setVisible(true);
//		rolAccion(rol, UserName);
//	}

	
	public void iniciarVentana() {
		frame.setVisible(true);
		rolAccion(rol, UserName);
	}

	
	public void closePrograma() {
		System.exit(0);
	}

	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1184, 727);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		tabbedPane();
		menuBarra();
		
		
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	public void menuBarra() {
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		mnFile = new JMenu("File");
		mnFile.setFont(new Font("Segoe UI", Font.BOLD, 16));
		menuBar.add(mnFile);
		{
			exportarMenu = new JMenu("Export");
			exportarMenu.setFont(new Font("Segoe UI", Font.BOLD, 16));
			menuBar.add(exportarMenu);
			{
				mntmImprimirTabla = new JMenuItem("Imprimir Tabla");
				mntmImprimirTabla.addActionListener(new ActionListener() {///////////////////////////////////////////////////////////////////////////////////
					public void actionPerformed(ActionEvent arg0) {
						
						if (rol.equals("invaite")) {
							JOptionPane.showMessageDialog(null, "No tiene permisos para imprimir");
						}else {
							Exportar imprimir =new Exportar();
							imprimir.setVisible(true);
						}
						
					}
				});
				mntmImprimirTabla.setFont(new Font("Segoe UI", Font.BOLD, 18));
				exportarMenu.add(mntmImprimirTabla);
			}
		}
		
	}
	
	
	@SuppressWarnings("unused")
	private void menueSalir(){
		
		mntmCerrarYSalir = new JMenuItem("Cerrar y Salir");
		mntmCerrarYSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		
		mnFile.add(mntmCerrarYSalir);
	}

	
	
	private void  menueItemUser_User() {
		
		mntmNewMenuItem = new JMenuItem("Cambio de Password");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TablaUserPassword changePassworOfUser = new TablaUserPassword(UserName);
				changePassworOfUser.inicializar();
				changePassworOfUser.setVisible(true);
			}
		});
		mnFile.add(mntmNewMenuItem);	
	}
	
	
	
	
	private void  menueItemAdmin_User() {
	 
		mntmNewMenuItem = new JMenuItem("Add User");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				TablaUserAdd addUser = new TablaUserAdd(rol);
				addUser.inicializar();
				addUser.setVisible(true);
			}
		});
		mnFile.add(mntmNewMenuItem);
		
		mntmNewMenuItem_1 = new JMenuItem("Auditar");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Auditoria uditar= new Auditoria(rol, UserName);
				uditar.setVisible(true);
				
			}
		});
		mnFile.add(mntmNewMenuItem_1);	 
 }
	
/////////////////////////////////////////////////////////////////////////////////////////////////	
	
	private void tabbedPane() {

		// tabbedPane.setEnabled(false);

		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());

		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addContainerGap()
						.addComponent(tabbedPane, GroupLayout.DEFAULT_SIZE, 1183, Short.MAX_VALUE).addContainerGap()));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(Alignment.TRAILING,
				groupLayout.createSequentialGroup().addContainerGap(50, Short.MAX_VALUE)
						.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 547, GroupLayout.PREFERRED_SIZE)
						.addContainerGap()));
		
		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		//Inventario(tabbedPane);//**************************************************************************
		
		
		//CargaInventaro(tabbedPane);//****************************************************************
		//salidaInventario(tabbedPane);//****************************************************************
		
		
///////////////////////////////////////////////////////////// **************************************************************	
		System.out.println(rol);//********************************************************************************************
	}

	/////////////////////////////////////////////////////////// metodos de los panel
	/////////////////////////////////////////////////////////// hijos//////////////////////////////////////////////////////////////////////////
	
	public void rolAccion(String roll, String Name) {
		
		System.out.println(rol);//*********************************************************************************************+

		inventario = new PanelInventario(rol, UserName);
		ingresoProducto = new PanelIngreso(rol, UserName);
		inventarioSalida = new PanellSalidaInventario(UserName, rol);
		
		
		
		if (rol.equals("admin")) {

			tabbedPane.setEnabled(true);
			inventario.Inventario(tabbedPane);
			ingresoProducto.CargaInventaro(tabbedPane);
			inventarioSalida.salidaInventario(tabbedPane);
			menueItemAdmin_User();
			menueItemUser_User(); 
			menueSalir();
			//Inventario(tabbedPane);
			//CargaInventaro(tabbedPane);
			//salidaInventario(tabbedPane);
			
			
		} else if (rol.equals("invaite")) {
			tabbedPane.setEnabled(true);
			inventario.Inventario(tabbedPane);
			menueSalir();
			//Inventario(tabbedPane);

		} else if (rol.equals("user")) {
			tabbedPane.setEnabled(true);
			
			inventario.Inventario(tabbedPane);
			ingresoProducto.CargaInventaro(tabbedPane);
			inventarioSalida.salidaInventario(tabbedPane);
			menueItemUser_User(); 
			menueSalir();
			//Inventario(tabbedPane);
			//CargaInventaro(tabbedPane);
			//salidaInventario(tabbedPane);
					
		}else if (rol.equals("user1")) {
			tabbedPane.setEnabled(true);
			
			inventario.Inventario(tabbedPane);
			inventarioSalida.salidaInventario(tabbedPane);
			menueItemUser_User(); 
			menueSalir();
		}else {
			System.out.println("no es usuario valido");
		}
	}

	
	private void salidaInventario(JTabbedPane tabbedPane) {
		
		JPanel salidaInventario = new JPanel();
		salidaInventario.addAncestorListener(new AncestorListener() {
			public void ancestorAdded(AncestorEvent event) {
				editar=false;
				ingreso=false;
				
			}
			public void ancestorMoved(AncestorEvent event) {
			}
			public void ancestorRemoved(AncestorEvent event) {
			}
		});
		
		tabbedPane.addTab("Salida de Inventario", null, salidaInventario, null);
		
		panel_2 = new JPanel();
		
		scrollPane_2 = new JScrollPane();
		
		table_2 = new JTable(model);
		table_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				
				if (!editar) {
					int seleccion2 =table_2.rowAtPoint(evt.getPoint());
					id_Salida_oculto.setText(String.valueOf(table_2.getValueAt(seleccion2, 0)));
					codigo_textField.setText(String.valueOf(table_2.getValueAt(seleccion2, 1)));
					areaalmacen.setText(String.valueOf(table_2.getValueAt(seleccion2, 3)));
					areaseccion.setText(String.valueOf(table_2.getValueAt(seleccion2, 4)));
					nota_textField.setText(String.valueOf(table_2.getValueAt(seleccion2, 7)));
					
				}else if (editar) {
					
					int seleccion3 =table_2.rowAtPoint(evt.getPoint());
					id_Salida_oculto.setText(String.valueOf(table_2.getValueAt(seleccion3, 0)));
					codigo_textField.setText(String.valueOf(table_2.getValueAt(seleccion3, 1)));
					cantidad_textField.setText(String.valueOf(table_2.getValueAt(seleccion3, 2)));
					areaalmacen.setText(String.valueOf(table_2.getValueAt(seleccion3, 3)));
					areaseccion.setText(String.valueOf(table_2.getValueAt(seleccion3, 4)));
					cliente_textField.setText(String.valueOf(table_2.getValueAt(seleccion3, 7)));
					nota_textField.setText(String.valueOf(table_2.getValueAt(seleccion3, 6)));
				}
				
			}
		});
		scrollPane_2.setViewportView(table_2);
		
		lblNewLabel_2 = new JLabel("Codigo del Producto");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		id_Salida_oculto = new JTextField();
		id_Salida_oculto.setColumns(10);
		id_Salida_oculto.setVisible(false);

		
		codigo_textField = new JTextField();
		codigo_textField.setColumns(10);
		
		lblCantidad = new JLabel("Cantidad");
		lblCantidad.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		
		cantidad_textField = new JTextField();
		cantidad_textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				ingreso=true;
			}
			@Override
			public void keyTyped(KeyEvent even) {
				char c = even.getKeyChar();
				if (!(Character.isDigit(c) || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
				JOptionPane.showMessageDialog(null,"No puede ingresar letras!!!","Ventana Error Datos",JOptionPane.ERROR_MESSAGE);
				even.consume();
				}
			}
		});
	
		
		cantidad_textField.setColumns(10);
		
		lblNota = new JLabel("Nota");
		lblNota.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		lblCliente = new JLabel("Cliente");
		lblCliente.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		cliente_textField = new JTextField();
		cliente_textField.setColumns(10);
		
		panel_3 = new JPanel();
		
		
		nota_textField = new JTextField();
		nota_textField.setColumns(10);
		
		areaalmacen = new JTextField();
		areaalmacen.setColumns(10);
		areaalmacen.setVisible(false);
		
		areaseccion = new JTextField();
		areaseccion.setVisible(false);
		areaseccion.setColumns(10);
		
		
		buscar_salida = new JTextField();
		buscar_salida.setColumns(10);
		
		lblSalidoDeProductos = new JLabel("SALIDA DE PRODUCTOS");
		lblSalidoDeProductos.setFont(new Font("Tahoma", Font.BOLD, 20));
		
		
		GroupLayout gl_salidaInventario = new GroupLayout(salidaInventario);
		gl_salidaInventario.setHorizontalGroup(
			gl_salidaInventario.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_salidaInventario.createSequentialGroup()
					.addGap(406)
					.addComponent(lblSalidoDeProductos, GroupLayout.PREFERRED_SIZE, 259, GroupLayout.PREFERRED_SIZE))
				.addGroup(Alignment.TRAILING, gl_salidaInventario.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 1127, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_salidaInventario.setVerticalGroup(
			gl_salidaInventario.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_salidaInventario.createSequentialGroup()
					.addGap(34)
					.addComponent(lblSalidoDeProductos)
					.addGap(18)
					.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 481, Short.MAX_VALUE)
					.addGap(48))
		);
		
		salidaInventario.setLayout(gl_salidaInventario);
		
		Botones();
		
		
	}
	
	
	
	public void clearInput() {
		id_Salida_oculto.setText("");
		codigo_textField.setText("");
		cantidad_textField.setText("");
		areaalmacen.setText("");
		areaseccion.setText("");
		cliente_textField.setText("");
		nota_textField.setText("");
	}
	
	
	public void Botones() {
		
		JCheckBox chckbxRestarALa = new JCheckBox("Restar a la Salida");
		JCheckBox sumarCheckBox = new JCheckBox("Sumar a la Salida");
		JButton Busqueda_Button_1 = new JButton("Buscar");
		
		Busqueda_Button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {///////////////////////////////////////////////////////////////////////////
				
				editar=false;
				corregir_panel.setVisible(false);
				AccionTabla accionTabla = new AccionTabla(model, buscar_salida.getText(), table_2);
				accionTabla.findTableOfType("inventario");
				
			}
		});
		
		
		JButton salida_Button = new JButton("Guardar");
		salida_Button.setBounds(198, 26, 97, 25);
		salida_Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String operacion="";
				
				if (ingreso && editar) {
					if(chckbxRestarALa.isSelected() || sumarCheckBox.isSelected() ) {
						
						if (chckbxRestarALa.isSelected()) {
							operacion="resta";
						}else if (sumarCheckBox.isSelected()) {
							operacion="suma";
						}
						
						AccionTabla accionTabla = new AccionTabla(model, buscar_salida.getText(), table_2
								,codigo_textField.getText(), areaalmacen.getText(), areaseccion.getText()
								,nota_textField.getText(), UserName, rol);
										
						cantidad = Integer.parseInt(cantidad_textField.getText());
						
						accionTabla.corregirSalida(cantidad, cliente_textField.getText(), operacion);			
						ingreso = false;
						editar =false;
						clearInput();
						corregir_panel.setVisible(false);
						salida_Button.setText("Guardar");
						
					}else {
						JOptionPane.showMessageDialog(null, "Para editar la salida debe de seleccionar si le resta o le suma al inventario");
					}
					
				}else if(ingreso) {
										
					AccionTabla accionTabla = new AccionTabla(model, buscar_salida.getText(), table_2
							,codigo_textField.getText(), areaalmacen.getText(), areaseccion.getText()
							,nota_textField.getText(), UserName, rol);
					
					cantidad = Integer.parseInt(cantidad_textField.getText());
					
					accionTabla.updateSalida("salida", cantidad, cliente_textField.getText());

					ingreso = false;
					editar =false;
					salida_Button.setText("Guardar");
					clearInput();

				}
			}
		});
		
		
		panel_3.setLayout(null);
		panel_3.add(salida_Button);
		
		salida_editar_Button = new JButton("Corregir");
		salida_editar_Button.setBounds(81, 26, 97, 25);
		salida_editar_Button.addActionListener(new ActionListener() {////////////////////////////***********************************************************////////////////////////
			public void actionPerformed(ActionEvent e) {
				editar=true;
				
				if (editar) {
					AccionTabla accionTabla = new AccionTabla(model, buscar_salida.getText(), table_2);
					accionTabla.findInventarioSalida("salida");
					corregir_panel.setVisible(true);
					salida_Button.setText("Salvar");
					
				}
			}
		});
		
		panel_3.add(salida_editar_Button);
		
		
		corregir_panel = new JPanel();
		corregir_panel.setBounds(12, 67, 339, 109);
		panel_3.add(corregir_panel);
		corregir_panel.setLayout(null);
		corregir_panel.setVisible(false);
		
		JLabel corregir_tituloLabel_3 = new JLabel("Acciones a Realizar");
		corregir_tituloLabel_3.setBounds(99, 13, 132, 16);
		corregir_panel.add(corregir_tituloLabel_3);
		
		
		sumarCheckBox.setBounds(8, 38, 139, 25);
		corregir_panel.add(sumarCheckBox);
		sumarCheckBox.addActionListener(new ActionListener() {///////////////////////////////***********************************************///////////////////////////////////////////
			
			public void actionPerformed(ActionEvent e) {
				if (sumarCheckBox.isSelected()) {
					chckbxRestarALa.setSelected(false);
				}

			}
		});
		
		
		chckbxRestarALa.setBounds(181, 38, 139, 25);
		corregir_panel.add(chckbxRestarALa);
		chckbxRestarALa.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				if ( chckbxRestarALa.isSelected()) {
					sumarCheckBox.setSelected(false);
				}	
			}
		});

		JButton cancelar_Button = new JButton("Cancelar");
		cancelar_Button.addActionListener(new ActionListener() {/////////////////////////*************************************************//////////////////////////////////////////
			public void actionPerformed(ActionEvent e) {
				
				corregir_panel.setVisible(false);
				editar=false;
				ingreso=false;
				salida_Button.setText("Guardar");
			}
		});
		cancelar_Button.setBounds(113, 72, 97, 25);
		corregir_panel.add(cancelar_Button);
		
		GroupLayoutSalidaInventarioBotones(Busqueda_Button_1);
		
	}
	

	
	public void GroupLayoutSalidaInventarioBotones(JButton Busqueda_Button_1) {
		
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(12)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addComponent(id_Salida_oculto, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
							.addGap(5)
							.addComponent(Busqueda_Button_1, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
							.addGap(30)
							.addComponent(buscar_salida, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(12)
							.addComponent(areaalmacen, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
							.addGap(65)
							.addComponent(areaseccion, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(25)
							.addComponent(lblNewLabel_2, GroupLayout.PREFERRED_SIZE, 156, GroupLayout.PREFERRED_SIZE)
							.addGap(12)
							.addComponent(codigo_textField, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(25)
							.addComponent(lblCantidad, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
							.addGap(75)
							.addComponent(cantidad_textField, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(25)
							.addComponent(lblCliente, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
							.addGap(75)
							.addComponent(cliente_textField, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(25)
							.addComponent(lblNota, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
							.addGap(75)
							.addComponent(nota_textField, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
						.addGroup(Alignment.TRAILING, gl_panel_2.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 372, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(scrollPane_2, GroupLayout.DEFAULT_SIZE, 710, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(23)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(1)
							.addComponent(id_Salida_oculto, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
						.addComponent(Busqueda_Button_1, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(4)
							.addComponent(buscar_salida, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)))
					.addGap(13)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(3)
							.addComponent(areaalmacen, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
						.addComponent(areaseccion, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
					.addGap(14)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(1)
							.addComponent(lblNewLabel_2, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
						.addComponent(codigo_textField, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
					.addGap(27)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(1)
							.addComponent(lblCantidad, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
						.addComponent(cantidad_textField, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
					.addGap(23)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(1)
							.addComponent(lblCliente, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
						.addComponent(cliente_textField, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
					.addGap(27)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(1)
							.addComponent(lblNota, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
						.addComponent(nota_textField, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
					.addGap(13)
					.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 189, GroupLayout.PREFERRED_SIZE)
					.addGap(13))
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(27)
					.addComponent(scrollPane_2, GroupLayout.DEFAULT_SIZE, 428, Short.MAX_VALUE)
					.addGap(26))
		);
		panel_2.setLayout(gl_panel_2);	
		
		
		
		
	}
	
	
	
	
	
	private void Inventario(JTabbedPane tabbedPane) {
		
		
		JPanel Inventario = new JPanel();

		tabbedPane.addTab("Inventario", null, Inventario, null);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		
		table_1 = new JTable(model);
		scrollPane_1.setViewportView(table_1);
		
		buscarInventario = new JTextField();
		buscarInventario.setColumns(10);
		
		JButton busqueda_boton = new JButton("Busqueda");
		busqueda_boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				AccionTabla accionTabla = new AccionTabla(model, buscarInventario.getText(), table_1);
				accionTabla.findTableOfType("inventario");
			}
		});
		
		JLabel tituloInventarioDeAlmacen = new JLabel("Inventario de Almacen");
		tituloInventarioDeAlmacen.setFont(new Font("Tahoma", Font.BOLD, 18));
		
		AccionTabla accionTabla = new AccionTabla(model, buscarInventario.getText(), table_1);
		accionTabla.findTableOfType("inventario");
		
		JButton Exportar_Button = new JButton("Exportar");
		Exportar_Button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				 try {
					 LibrosExcel inventariobook = new  LibrosExcel();
					 
					inventariobook.createBookInventario("inventario", buscarInventario.getText());
				} catch (Throwable e) {
					e.printStackTrace();
					System.out.println("ocurrio un error al crear el book");
				}
			}
		});
		
		if (rol!="invite" || rol==null) {
			Exportar_Button.setVisible(true);
		}else {
			Exportar_Button.setVisible(false);
		}
		
		GroupLayout gl_Inventario = new GroupLayout(Inventario);
		gl_Inventario.setHorizontalGroup(
			gl_Inventario.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_Inventario.createSequentialGroup()
					.addGap(430)
					.addComponent(tituloInventarioDeAlmacen, GroupLayout.PREFERRED_SIZE, 224, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_Inventario.createSequentialGroup()
					.addGap(32)
					.addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 1088, Short.MAX_VALUE)
					.addGap(37))
				.addGroup(gl_Inventario.createSequentialGroup()
					.addGap(32)
					.addComponent(buscarInventario, GroupLayout.PREFERRED_SIZE, 224, GroupLayout.PREFERRED_SIZE)
					.addGap(33)
					.addComponent(busqueda_boton)
					.addGap(29)
					.addComponent(Exportar_Button))
		);
		gl_Inventario.setVerticalGroup(
			gl_Inventario.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_Inventario.createSequentialGroup()
					.addGap(35)
					.addComponent(tituloInventarioDeAlmacen, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
					.addGap(13)
					.addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 411, Short.MAX_VALUE)
					.addGap(24)
					.addGroup(gl_Inventario.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_Inventario.createSequentialGroup()
							.addGap(1)
							.addComponent(buscarInventario, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(busqueda_boton)
						.addComponent(Exportar_Button))
					.addGap(63))
		);
		Inventario.setLayout(gl_Inventario);
		
		
	}
	

	
	private  void clearCeldas() {
		 	 codigotxt.setText("");
			 textField_cantidad.setText("");
			 textField_areaalmacen.setText("");
			 textField_areaseccion.setText("");
			 textField_nota.setText("");
	}
	

		
	
	/*private void CargaInventaro(JTabbedPane tabbedPane) {
		
		JPanel CargaInventaro = new JPanel();
		CargaInventaro.addAncestorListener(new AncestorListener() {
			public void ancestorAdded(AncestorEvent arg0) {
				editar=false;
				ingreso=false;

			}
			public void ancestorMoved(AncestorEvent arg0) {
			}
			public void ancestorRemoved(AncestorEvent arg0) {
			}
		});

		
		tabbedPane.addTab("Carga de Inventario", null, CargaInventaro, null);
		//tabbedPane.setBackgroundAt(1, new Color(255, 255, 255));
		
		panel_contenedor1 = new JPanel();
		
		lblNewLabel_1 = new JLabel("Codigo Producto");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		lblCantidadProducto_1 = new JLabel("Cantidad");
		lblCantidadProducto_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		lblAreaDeAlmacenaje_1 = new JLabel("Area de Almacen");
		lblAreaDeAlmacenaje_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		lblSeccinDeAlmacenaje_1 = new JLabel("Secci\u00F3n de Almacen");
		lblSeccinDeAlmacenaje_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		notalabel = new JLabel("Nota");
		notalabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		codigotxt = new JTextField();
		codigotxt.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				idII=1;
			}
		});
		codigotxt.setColumns(10);
		
		textField_cantidad = new JTextField();
		textField_cantidad.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				ingreso=true;
			}
		});
		textField_cantidad.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent even) {
				
				char c = even.getKeyChar();
				if (!(Character.isDigit(c) || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
				JOptionPane.showMessageDialog(null,"No puede ingresar letras!!!","Ventana Error Datos",JOptionPane.ERROR_MESSAGE);
				even.consume();
				}
			}
		});
		textField_cantidad.setColumns(10);
		
		textField_areaalmacen = new JTextField();
		textField_areaalmacen.setColumns(10);
		
		textField_areaseccion = new JTextField();
		textField_areaseccion.setColumns(10);
		
		id_oculto = new JTextField();
		id_oculto.setColumns(10);
		id_oculto.setVisible(false);
		
		table = new JTable(model);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {//evento de seleccion 
					
				if (editar) {
					int seleccion =table.rowAtPoint(evt.getPoint());
					id_oculto.setText(String.valueOf(table.getValueAt(seleccion, 0)));
					codigotxt.setText(String.valueOf(table.getValueAt(seleccion, 1)));
					textField_cantidad.setText(String.valueOf(table.getValueAt(seleccion, 2)));
					textField_areaalmacen.setText(String.valueOf(table.getValueAt(seleccion, 3)));
					textField_areaseccion.setText(String.valueOf(table.getValueAt(seleccion, 4)));
					textField_nota.setText(String.valueOf(table.getValueAt(seleccion, 7)));
				}
			}
		});
		scrollPane = new JScrollPane();
		scrollPane.setViewportView(table);
		
		textField_nota = new JTextField();
		textField_nota.setColumns(10);
	
		buscar = new JTextField();
		buscar.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent even) {
				
				//int id=Integer.parseInt(id_oculto.getText());    
				
				AccionTabla accionTabla = new AccionTabla(model, buscar.getText(), table, codigotxt.getText(), textField_areaalmacen.getText(), textField_areaseccion.getText()
						, textField_nota.getText(), UserName, rol);
				accionTabla.findInventario();
				
			}
		});
		buscar.setColumns(10);
		
		JLabel lblBuscarPor = new JLabel("Buscar por");
		lblBuscarPor.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		
		panelBotones(2);
		
		//botonDentroIngreso(panel_1);
		//id_oculto.setText("0");
		//int id=Integer.parseInt(id_oculto.getText()); 
		
		AccionTabla accionTabla = new AccionTabla(model, buscar.getText(), table, codigotxt.getText(), textField_areaalmacen.getText(), textField_areaseccion.getText()
				, textField_nota.getText(), UserName, rol);
		
		accionTabla.findTableOfType("inventario");
		
		lblCargaDeProducto = new JLabel("CARGA DE PRODUCTO A INVENTARIO");
		lblCargaDeProducto.setFont(new Font("Tahoma", Font.BOLD, 20));
		
////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
		GroupLayout(CargaInventaro, lblNewLabel_1, lblCantidadProducto_1
				,lblAreaDeAlmacenaje_1, lblSeccinDeAlmacenaje_1, notalabel,ingresarButton , textField_nota, panel_contenedor1,lblBuscarPor,panel_1 );
		
		
	
	}
	
	public void panelBotones(int numero) {
		
		panel_1 = new JPanel();
		panel_1.setLayout(null);
		
		if (numero ==2) {
			botonDentroIngreso(panel_1);
		}
	}
		
	
	
	
	public void botonDentroIngreso(JPanel panel_1) {
			
		Borrar_Button = new JButton("Borrar");
		Borrar_Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				//int id=Integer.parseInt(id_oculto.getText()); 
				
				AccionTabla accionTabla = new AccionTabla(model, buscar.getText(), table, codigotxt.getText()
						, textField_areaalmacen.getText(), textField_areaseccion.getText()
						, textField_nota.getText(), UserName, rol);
				
				accionTabla.eliminarTodaLaTabla();
				JOptionPane.showMessageDialog(null,"Campo borrrado","Ventana Error Datos",JOptionPane.ERROR_MESSAGE);
				accionTabla.findTableOfType("inventario");
				clearCeldas();
			}
		});
		Borrar_Button.setBounds(228, 65, 97, 25);
		panel_1.add(Borrar_Button);
		Borrar_Button.setVisible(false);
		
		
		cancelarButton = new JButton("Salir de editar");
		cancelarButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				editar=false;	
				ingreso=false;
				EditarSalvarButton.setVisible(false);
				cancelarButton.setVisible(false);
				Borrar_Button.setVisible(false);
				ingresarButton.setEnabled(true);
				clearCeldas();
			}
		});
		cancelarButton.setBounds(133, 111, 141, 25);
		panel_1.add(cancelarButton);
		cancelarButton.setVisible(false);
		
		
		editarButton = new JButton("Editar");
		editarButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				editar=true;
				EditarSalvarButton.setVisible(true);
				cancelarButton.setVisible(true);
				Borrar_Button.setVisible(true);
			}
		});
		editarButton.setVisible(true);
		editarButton.setBounds(92, 27, 97, 25);
		panel_1.add(editarButton);
		

		EditarSalvarButton = new JButton("Guardar cambios");
		EditarSalvarButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actioneditar) {
				
				if (ingreso || idII == 1) {
					
					//int id=Integer.parseInt(id_oculto.getText()); 
					
					AccionTabla accionTabla = new AccionTabla(model, buscar.getText(), table, codigotxt.getText()
							, textField_areaalmacen.getText(), textField_areaseccion.getText()
							, textField_nota.getText(), UserName, rol);
					
					cantidad = Integer.parseInt(textField_cantidad.getText());
					accionTabla.botonDeSalveEdition(UserName, rol, cantidad, Integer.parseInt(id_oculto.getText())); 
					
					//JOptionPane.showMessageDialog(null,"","Ventana Error Datos",JOptionPane.ERROR_MESSAGE);	
					accionTabla.findTableOfType("inventario");
					clearCeldas();
				}
			}
		});
		EditarSalvarButton.setBounds(49, 65, 148, 25);
		panel_1.add(EditarSalvarButton);
		EditarSalvarButton.setVisible(false);
		
			
		ingresarButton = new JButton("Ingresar");
		ingresarButton.setVisible(true);
		ingresarButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if (ingreso && !editar) {
					
					//int id=Integer.parseInt(id_oculto.getText()); 
					
					AccionTabla accionTabla = new AccionTabla(model, buscar.getText(), table, codigotxt.getText()
							, textField_areaalmacen.getText(), textField_areaseccion.getText()
							, textField_nota.getText(), UserName, rol);
					
					cantidad = Integer.parseInt(textField_cantidad.getText());
					accionTabla.updateInventario("inventario", cantidad,table);
					clearCeldas();
					ingreso=false;
				}else if (!ingreso && editar){
					JOptionPane.showMessageDialog(null,"Esta en modo editar la funsionabilidad de ingreso se activa al salvar las edici�n","Ventana Error Datos",JOptionPane.ERROR_MESSAGE);
				}else {
					JOptionPane.showMessageDialog(null,"Llene todos los campos","Ventana Error Datos",JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		ingresarButton.setBounds(218, 27, 95, 25);
		panel_1.add(ingresarButton);
		
	}
	
	
	private void GroupLayout(  JPanel CargaInventaro,JLabel lblNewLabel,JLabel lblCantidadProducto
			,JLabel lblAreaDeAlmacenaje, JLabel lblSeccinDeAlmacenaje, JLabel lblFechaDeIngreso, JButton btnNewButton,JTextField textField_nota,JPanel panel,JLabel lblBuscarPor,JPanel panel_1) {
	
		GroupLayout gl_CargaInventaro = new GroupLayout(CargaInventaro);
		gl_CargaInventaro.setHorizontalGroup(
			gl_CargaInventaro.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_CargaInventaro.createSequentialGroup()
					.addGap(344)
					.addComponent(lblCargaDeProducto, GroupLayout.PREFERRED_SIZE, 401, GroupLayout.PREFERRED_SIZE))
				.addComponent(panel_contenedor1, GroupLayout.DEFAULT_SIZE, 1111, Short.MAX_VALUE)
		);
		gl_CargaInventaro.setVerticalGroup(
			gl_CargaInventaro.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_CargaInventaro.createSequentialGroup()
					.addGap(25)
					.addComponent(lblCargaDeProducto, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
					.addGap(15)
					.addComponent(panel_contenedor1, GroupLayout.DEFAULT_SIZE, 476, Short.MAX_VALUE)
					.addGap(63))
		);
		GroupLayout gl_panel_contenedor1 = new GroupLayout(panel_contenedor1);
		gl_panel_contenedor1.setHorizontalGroup(
			gl_panel_contenedor1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_contenedor1.createSequentialGroup()
					.addGap(27)
					.addGroup(gl_panel_contenedor1.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel_contenedor1.createSequentialGroup()
							.addGroup(gl_panel_contenedor1.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_contenedor1.createSequentialGroup()
									.addGap(29)
									.addComponent(lblBuscarPor, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
									.addGap(54)
									.addComponent(buscar, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel_contenedor1.createSequentialGroup()
									.addGap(218)
									.addComponent(id_oculto, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel_contenedor1.createSequentialGroup()
									.addGap(29)
									.addComponent(lblNewLabel_1)
									.addGap(72)
									.addComponent(codigotxt, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel_contenedor1.createSequentialGroup()
									.addGap(29)
									.addComponent(lblCantidadProducto_1)
									.addGap(127)
									.addComponent(textField_cantidad, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel_contenedor1.createSequentialGroup()
									.addGap(29)
									.addComponent(lblAreaDeAlmacenaje_1)
									.addGap(74)
									.addComponent(textField_areaalmacen, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel_contenedor1.createSequentialGroup()
									.addGap(29)
									.addComponent(lblSeccinDeAlmacenaje_1)
									.addGap(54)
									.addComponent(textField_areaseccion, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel_contenedor1.createSequentialGroup()
									.addGap(29)
									.addComponent(notalabel)
									.addGap(155)
									.addComponent(textField_nota, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)))
							.addGap(8))
						.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 352, GroupLayout.PREFERRED_SIZE))
					.addGap(29)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 663, Short.MAX_VALUE)
					.addGap(36))
		);
		gl_panel_contenedor1.setVerticalGroup(
			gl_panel_contenedor1.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panel_contenedor1.createSequentialGroup()
					.addGroup(gl_panel_contenedor1.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel_contenedor1.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_panel_contenedor1.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_contenedor1.createSequentialGroup()
									.addGap(3)
									.addComponent(lblBuscarPor, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
								.addComponent(buscar, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
							.addGap(13)
							.addComponent(id_oculto, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
							.addGap(9)
							.addGroup(gl_panel_contenedor1.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_contenedor1.createSequentialGroup()
									.addGap(2)
									.addComponent(lblNewLabel_1))
								.addComponent(codigotxt, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
							.addGap(14)
							.addGroup(gl_panel_contenedor1.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_contenedor1.createSequentialGroup()
									.addGap(2)
									.addComponent(lblCantidadProducto_1))
								.addComponent(textField_cantidad, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
							.addGap(18)
							.addGroup(gl_panel_contenedor1.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_contenedor1.createSequentialGroup()
									.addGap(2)
									.addComponent(lblAreaDeAlmacenaje_1))
								.addComponent(textField_areaalmacen, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
							.addGap(20)
							.addGroup(gl_panel_contenedor1.createParallelGroup(Alignment.LEADING)
								.addComponent(lblSeccinDeAlmacenaje_1)
								.addGroup(gl_panel_contenedor1.createSequentialGroup()
									.addGap(4)
									.addComponent(textField_areaseccion, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)))
							.addGap(13)
							.addGroup(gl_panel_contenedor1.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_contenedor1.createSequentialGroup()
									.addGap(2)
									.addComponent(notalabel))
								.addComponent(textField_nota, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
							.addGap(18)
							.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 174, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_contenedor1.createSequentialGroup()
							.addGap(13)
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE)))
					.addGap(13))
		);
		panel_contenedor1.setLayout(gl_panel_contenedor1);
		CargaInventaro.setLayout(gl_CargaInventaro);
	}*/
	
	
	
	
	
	
	
	
	

	public static String getRol() {
		return rol;
	}

	public static void setRol(String rol) {
		Windowsprincipal.rol = rol;
	}

	public static String getUserName() {
		return UserName;
	}

	public static void setUserName(String userName) {
		UserName = userName;
	}
}
