package excel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.swing.JOptionPane;
import javax.swing.filechooser.FileSystemView;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.view.panel.AccionTabla;
import com.view.panel.ModelMetodosJTable;



public class LibrosExcel {


	public void createBlanckBookUser() throws Throwable {

		
		//Create blank workbook
	      XSSFWorkbook workbook = new XSSFWorkbook(); 

	      //Create a blank sheet crea la hoja 
	      XSSFSheet spreadsheet = workbook.createSheet(" User Info ");

	      //Create row object
	      XSSFRow row;
	     
	      ModelMetodosJTable excel = new ModelMetodosJTable();
	      
	     Map < String, Object[] > empinfo = excel.empinfoMap("");
	      
	      Set < String > keyid =empinfo.keySet();
	      
	      int rowid = 0;

	      for (String key : keyid) {
	         row = spreadsheet.createRow(rowid++);
	         Object [] objectArr = empinfo.get(key);
	         int cellid = 0;

	         
	         for (Object obj : objectArr) {
	            Cell cell = row.createCell(cellid++);
	            cell.setCellValue(obj.toString());
	         }
	      }

	      File home2 = FileSystemView.getFileSystemView().getHomeDirectory(); 
	      //codigo pra colocar en escritorio el archivo
	      String absPath2 = home2.getAbsolutePath();
	      //Write the workbook in file system
	      FileOutputStream out = new FileOutputStream(new File(absPath2+"/Listado_de_User.xlsx"));
	      workbook.write(out);
	      out.close();
	      System.out.println("Writesheet.xlsx written successfully");	
	}


	
	
	public void createBookInventario(String nameOfTable, String buscarpor) throws Throwable {

		//Create blank workbook
	      XSSFWorkbook workbook = new XSSFWorkbook(); 

	      //Create a blank sheet crea la hoja 
	      XSSFSheet spreadsheet = workbook.createSheet(" User Info ");

	      //Create row object
	      XSSFRow row;
	      
	      AccionTabla excelInventario = new AccionTabla();
	      
	      
	     Map < String, Object[] > empinfo = excelInventario.empinfoMapInventario(nameOfTable, buscarpor);
	      
	      Set < String > keyid =empinfo.keySet();
	      
	      int rowid = 0;

	      for (String key : keyid) {
	         row = spreadsheet.createRow(rowid++);
	         Object [] objectArr = empinfo.get(key);
	         int cellid = 0;

	         
	         for (Object obj : objectArr) {
	            Cell cell = row.createCell(cellid++);
	            cell.setCellValue(obj.toString());
	         }
	      }

	      File home = FileSystemView.getFileSystemView().getHomeDirectory(); 
	      //codigo pra colocar en escritorio el archivo
	      String absPath = home.getAbsolutePath();
			
	      
	      //Write the workbook in file system
	      FileOutputStream out = new FileOutputStream(new File(absPath+"/Lista_"+nameOfTable+".xlsx"));
	      workbook.write(out);
	      out.close();
	     // System.out.println("Writesheet.xlsx written successfully");	
	      JOptionPane.showMessageDialog(null, "Finalizo Exportacion a Escritorio");
	      
	      
	}
	
	
	
}
